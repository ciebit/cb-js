/*
==================================
			CIEBIT
==================================
Criada: 21/01/2014
Altera: 17/02/2014
----------------------------------
*/

//Criando funções para data
Date.prototype.getDia = function()
{
	var num = this.getDate();

	if(num < 10){
		num = '0' + num.toString();
	}

	return num;
}
Date.prototype.getMes = function()
{
	var num = this.getMonth();
	num = num + 1;

	if(num < 10){
		num = '0' + num.toString();
	}

	return num;
}
Date.prototype.getMesAbr = function()
{
	switch(this.getMonth()){
		case 0:  return 'Jan';
		case 1:  return 'Fev';
		case 2:  return 'Mar';
		case 3:  return 'Abr';
		case 4:  return 'Mai';
		case 5:  return 'Jun';
		case 6:  return 'Jul';
		case 7:  return 'Ago';
		case 8:  return 'Set';
		case 9:  return 'Out';
		case 10: return 'Nov';
		case 11: return 'Dez';
		default: return '';
	}
}
Date.prototype.getMesExtenso = function()
{
	switch(this.getMonth()){
		case 0:  return 'Janeiro';
		case 1:  return 'Fevereiro';
		case 2:  return 'Março';
		case 3:  return 'Abril';
		case 4:  return 'Maio';
		case 5:  return 'Junho';
		case 6:  return 'Julho';
		case 7:  return 'Agosto';
		case 8:  return 'Setembro';
		case 9:  return 'Outrubro';
		case 10: return 'Novembro';
		case 11: return 'Dezembro';
		default: return '';
	}
}
Date.prototype.getData = function()
{
	var ano = this.getFullYear();
	var mes = this.getMes();
	var dia = this.getDia();

	return dia +'/'+ mes +'/'+ ano;
}
Date.prototype.getTempo = function(formato)
{
	var hor = this.getHours();
	var min = this.getMinutes();
	var seg = this.getSeconds();

	if(hor < 10) hor = '0'+ hor.toString();
	if(min < 10) min = '0'+ min.toString();
	if(seg < 10) seg = '0'+ seg.toString();

	// Se não hover formato retornar padrão
	if(!(typeof formato === 'string')) {
		return hor +':'+ min +':'+ seg;
	}

	// Substituindo padrões
	var horario = formato.replace('h', hor);
	horario = horario.replace('m', min);
	return horario.replace('s', seg);

}
Date.prototype.getDataInvertida = function()
{
	var ano = this.getFullYear();
	var mes = this.getMes();
	var dia = this.getDia();

	return ano +'-'+ mes +'-'+ dia;
}
Date.prototype.getDataHora = function()
{
	var data = this.getData();
	var hora = this.getTempo();

	data += ' '+ hora;

	return data;
}
Date.prototype.getSemanaExtenso = function()
{
	switch(this.getDay()){
		case 0:  return 'Domingo';
		case 1:  return 'Segunda-feira';
		case 2:  return 'Terça-feira';
		case 3:  return 'Quarta-feira';
		case 4:  return 'Quinta-feira';
		case 5:  return 'Sexta-feira';
		case 6:  return 'Sábado';
		default: return '';
	}
}
Date.prototype.obterIntervalo = function( data, formato )
{
	if( !(data instanceof Date) ) return false;

	var diferenca = this.getTime() - data.getTime();

	// Tornando positivo caso negativo
	if( diferenca < 0 ) diferenca = diferenca * -1;

	switch ( formato ) {
		case 's': return Math.round( diferenca / 1000 );
		case 'm': return Math.round( diferenca / 1000 / 60 );
		case 'h': return Math.round( diferenca / 1000 / 60 / 60 );
		case 'd': return Math.round( diferenca / 1000 / 60 / 60 / 24 );
		default: return diferenca;
	}
}
Date.prototype.setData = function(ano, mes, dia)
{
	// Zerando
	this.setMonth(0);
	this.setDate(1);

	// Configurando
	this.setFullYear(ano);
	this.setMonth(mes-1);
	this.setDate(dia);

	return this;
}
Date.prototype.setDataHora = function(dataHora)
{
	//Encerrar se não houver hora
	if(!dataHora) return this;

	//Separando elementos
	var expr = /(\d+)[-\/]{1}(\d+)[-\/]{1}(\d+)(\s(\d{1,2}):(\d{1,2}):(\d{1,2}))?/;
	var temp = expr.exec(dataHora);

	// Se não encontrar nada encerrar
	if(!Array.isArray(temp)) return this;

	//Transformando em números
	var ano = parseInt(temp[3]),
		mes = parseInt(temp[2]),
		dia = parseInt(temp[1]),
		hor = parseInt(temp[5]),
		min = parseInt(temp[6]),
		seg = parseInt(temp[7]);

	//Inverter se estiver trocado
	if(ano < dia){
		var i = dia;
		dia = ano;
		ano = i;
	}

	//Definindo data se houver
	ano && this.setFullYear(ano);
	(mes > 0) && this.setMonth(mes -1);
	dia && this.setDate(dia);

	//Definindo horário se houver
	if(hor || hor == 0) this.setHours(hor);
	if(min || min == 0) this.setMinutes(min);
	if(seg || seg == 0) this.setSeconds(seg);

	return this;
}
Date.prototype.setTempo = function(tempo)
{
	// Sem parâmetro encerrar
	if(
		!tempo ||
		(typeof tempo !== 'string')
	) {
		return this;
	}

	// Separando elementos
	var arr = tempo.split(':', 3),
	h = parseInt(arr[0]),
	m = parseInt(arr[1]),
	s = parseInt(arr[2]);

	if(!h) h = 0;
	if(!m) m = 0;
	if(!s) s = 0;

	this.setHours(h,m,s);

	return this;
}
