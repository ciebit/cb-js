/*
==================================
			CIEBIT
==================================
Criada: 03/07/2015
Altera: 03/07/2015
----------------------------------
Dependências:
- f.hermes
- f.classes
- e.datas
*/

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

cb.o.calendario = (function() 
{
	var __contadorId = 0;

	var f = function()
	{
		this.objId = 'cb-o-calendario-'+ __contadorId++;
		this.dom = {
			'recipiente': undefined,
			'tabela'    : undefined,
			'cabData'   : undefined,
			'btProximo' : undefined,
			'btAnterior': undefined,
			'dias'      : []
		};
		this.situacao = {
			'instalado': false
		}
		this.dia = undefined;
		this.mes = undefined;
		this.ano = undefined;
		this.tempo = new Date;
		this.classeDia         = 'cb-cal-dia';
		this.classeInativo     = 'cb-cal-inativo';
		this.classeMarcado     = 'cb-cal-marcado';
		this.classeDomingo     = 'cb-cal-domingo';
		this.classeSelecionado = 'cb-cal-selecionado';
	};

	/*
	 * Atualiza o calendário exibindo os dias conforme
	 * o mês do ano selecionado
	*/
	f.prototype.Atualizar = function(_ano, _mes, _dia)
	{
		var Data = new Date,
		semana, dia, mes, ano,
		diaAtual, mesAtual, anoAtual,
		diaMesAnt, diaMesAtual, mesAnt,
		dataExtenso;
		
		if(_ano instanceof Date) Data = _ano;
		else {
			// Verificando se especificou mês e ano e inserindo
			if(_ano != undefined) Data.setFullYear(_ano);
			if(_mes != undefined) Data.setMonth(_mes -1);
			if(_dia != undefined) Data.setDate(_dia);
		}

		// Coletando dados importantes
		// Armazenando as datas atuais
		diaAtual = Data.getDate();
		mesAtual = Data.getMonth();
		anoAtual = Data.getFullYear();

		// Repassando para o objeto
		this.dia = diaAtual;
		this.mes = mesAtual;
		this.ano = anoAtual;
		this.tempo.setData(anoAtual, mesAtual+1, diaAtual);

		// Atualizando barra
		this.dom.cabData.removeChild(this.dom.cabData.lastChild);
		dataExtenso = Data.getMesExtenso() +'/'+ anoAtual;
		this.dom.cabData.appendChild(document.createTextNode(dataExtenso));

		// Descobrindo em que dia da semana o mês começa
		Data.setDate(1);
		semana = Data.getDay(); 

		// Descobrindo o último dia do mês anterior
		Data.setDate(0);
		diaMesAnt = Data.getDate();
		mesAnt = Data.getMonth();
		mes = mesAnt;
		ano = Data.getFullYear();

		// Descobrindo o último dia do mês atual
		Data.setData(anoAtual, mesAtual+2, 0);
		diaMesAtual = Data.getDate();

		// Configurando número inicial
		dia = diaMesAnt - semana +1;

		// Alimentando dados
		for(var i = 0; i < 42; i++)
		{
			var classe;

			// Verificando dia
			if(
				(dia > diaMesAnt   && mes == mesAnt) ||
				(dia > diaMesAtual && mes == mesAtual)
			){
				dia = 1;
				mes++;
			}

			// Verificando mês
			if(mes > 11){
				mes = 0;
				ano++
			}

			// Verificando se os dias são fora do mês atual
			if(mes != mesAtual) classe = 'cb-cal-inativo';
			else classe = '';

			if(dia == this.dia) classe += ' cb-cal-selecionado';

			// Configurando
			Data.setData(ano, mes+1, dia);
			var txt = document.createTextNode(dia);
			this.dom.dias[i].removeChild(this.dom.dias[i].lastChild);
			this.dom.dias[i].appendChild(txt);
			this.dom.dias[i].className = classe;
			this.dom.dias[i].href = '?data='+ Data.getDataInvertida();
			dia++;
		}

		// Informando atualização
		cb.f.hermes.informe(this.objId +'-atualizado', this.tempo);
	}

	/*
	 * Marca uma função para ser chamada em determinado evento
	*/
	f.prototype.AviseMe = function(evento, func, ref, unica)
	{
		cb.f.hermes.aviseMe(
			func, this.objId +'-'+ evento, 
			ref, unica
		);
		return this;
	};

	// Retorna o dia da semana abreviado conforme
	// representação númerica do javascript
	f.prototype.DiaSemana = function(sem)
	{
		switch(sem){
			case 0:  return 'DOM';
			case 1:  return 'SEG';
			case 2:  return 'TER';
			case 3:  return 'QUA';
			case 4:  return 'QUI';
			case 5:  return 'SEX';
			case 6:  return 'SAB';
			default: return '';
		}
	}

	/*
	 * Criao os elementos e adiciona eventos
	*/
	f.prototype.Instalar = function()
	{
		// Se já tiver sido instalado, encerrar
		if(this.situacao.instalado) return;

		// Criando elementos
		var barra  = document.createElement('div'),
		btAnterior = document.createElement('a'),
		btProximo,
		cabData    = document.createElement('p'),
		cabDataTxt = document.createTextNode('Mês/Ano'),
		tabela     = document.createElement('table'),
		tCabecalho = document.createElement('thead'),
		tCorpo     = document.createElement('tbody'),
		tLinha     = document.createElement('tr'),
		tCelula    = document.createElement('td'),
		tCelSemana = document.createElement('th'),
		diaLink    = document.createElement('a');

		// Botões - Configurando
		btAnterior.textContent = 'Anterior';
		btAnterior.className = 'cb-cal-bts_mes';

		btProximo = btAnterior.cloneNode();
		btProximo.textContent = 'Próximo';

		// Adicionando classes
		tabela.className     = 'cb-calendario';
		tCelula.className    = this.classeDia;
		barra.className      = 'cb-cal-cabecalho';
		cabData.className    = 'cb-cal-mes_ano';
		btProximo.className  += ' cb-cal-bt_proximo';
		btAnterior.className += ' cb-cal-bt_anterior';

		// Infomração mês e ano
		cabData.appendChild(cabDataTxt);
		
		// Inserindo Barra Superior
		barra.appendChild(btAnterior);
		barra.appendChild(cabData);
		barra.appendChild(btProximo);
		this.dom.recipiente.appendChild(barra);

		// Criando tabela

		// Criando cabeçalho
		var lin = tLinha.cloneNode();
		tCabecalho.appendChild(lin);

		// Criando dias da semana
		for (var i = 0; i < 7; i++) 
		{
			var cel = tCelSemana.cloneNode(),
			    txt = document.createTextNode(this.DiaSemana(i));
			cel.appendChild(txt);
			lin.appendChild(cel);
		};

		// Criando celulas da tabela para os dias
		var dom = ' '+this.classeDomingo,
		    lin = tLinha.cloneNode();
		diaLink.appendChild(document.createTextNode('-'));
		for(var i = 0; i < 42; i++)
		{
			var cel = tCelula.cloneNode(),
			    d   = diaLink.cloneNode(true);


		    // Verificando momento de mudança de linha
		    if(
		    	i ==  7 || i == 14 || 
		    	i == 21 || i == 28 ||
		    	i == 35
		    ){
		    	lin = tLinha.cloneNode();
		    	dom = ' '+this.classeDomingo;
		    }
		    
			// Adicionado domingo
			cel.className += dom;
			dom = '';

			cel.appendChild(d);
			lin.appendChild(cel);
			tCorpo.appendChild(lin);
			this.dom.dias.push(d);
		}


		// Inserindo tabela
		tabela.appendChild(tCabecalho);
		tabela.appendChild(tCorpo);
		this.dom.recipiente.appendChild(tabela);

		// Guardandno referências
		this.dom.tabela = tabela;
		this.dom.btAnterior = btAnterior;
		this.dom.btProximo = btProximo;
		this.dom.cabData = cabData;


		// Eventos
		var self = this;
		btProximo.addEventListener('click', function(){
			self.Atualizar(self.ano, self.mes+2);
		});
		btAnterior.addEventListener('click', function(){
			self.Atualizar(self.ano, self.mes);
		});
		tabela.addEventListener('click', function(evt)
		{
			evt.preventDefault();
			var alvo = evt.target;

			if(
				alvo instanceof HTMLElement &&
				alvo.tagName.toLowerCase() == 'a'
			) {
				self.LimparSelecao();
				cb.f.ClasseAdi(alvo, 'cb-cal-selecionado');
				self.dia = alvo.textContent;
				var Data = new Date;
				Data.setDataHora(alvo.href);
				cb.f.hermes.informe(self.objId+'-alteracao-dia', Data);
			}
		});

		// Infornando conclusão na instalação
		this.situacao.instalado = true;

		// Atualizando Calendário
		this.Atualizar();
	};

	/*
	 * Remove a seleção de dia
	*/
	f.prototype.LimparSelecao = function()
	{
		var self = this;

		// Percorrendo itens
		this.dom.dias.forEach(function(dia){
			cb.f.ClasseExc(dia, 'cb-cal-selecionado');
		});
	}

	/*
	 * Marca um determinado dia da agenda
	 * Recebe: um número que represente um dia do mês atual
	 * Retorna: Retorna o nó alterado
	*/
	f.prototype.Marcar = function(d)
	{
		if(d < 1 || d > 31) return;

		// Percorrendo dias para marcar o correto
		var i;
		for(i in this.dom.dias)
		{
			var elem = this.dom.dias[i],
				dia  = elem.textContent;

			if(cb.f.ClasseTem(elem, this.classeInativo)) continue;
			if(dia != d) continue;

			cb.f.ClasseAdi(elem, this.classeMarcado);
			return elem;
		}
	}

	/*
	 * Retorna o nó relativo ao dia informado
	*/
	f.prototype.NoDia = function(d)
	{
		if(d < 1 || d > 31) return;

		// Buscando o dia informado
		var i;
		for(i in this.dom.dias)
		{
			var elem = this.dom.dias[i],
				dia  = elem.textContent;
			if(dia != d) continue;

			return elem;
		}
	}

	/*
	 * Retorna um objeto data com o dia selecinado
	 * Caso não haja dia selecionado, retorna false
	*/
	f.prototype.Valor = function()
	{
		if( !this.dia || !this.mes || !this.ano )
		{
			return false;
		}

		var Data = new Date;
		Data.setData(this.ano, this.mes+1, this.dia);
		return Data;
	}

	return f;
})();