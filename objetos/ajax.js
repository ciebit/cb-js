/*
==================================
------------- CIEBIT -------------
==================================
Dependências:
- cb.o.cbjson
*/

cb.o.ajax = (function()
{
	var __contadorId = 0;
	var __modoDepuracao = false;
	var	self = function()
	{
		this.id       = 'cb-o-ajax-'+ __contadorId++;
		this.abortado = false; //Informa se foi solicitado o aborto
		this.conexao  = false;
		this.metodo   = 'GET';
		this.url      = '';
		this.assinc   = 'true';
		this.formato  = 'json';
		this.dados    = null;
		this.encType  = 'application/x-www-form-urlencoded; charset=UTF-8';
		this.modoDepuracao = false;
		this.msg_id   = 0;
		this.msg_lista= _msg();
	}

	/*
	 * Função:  Cancela conexão atual
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Encerra
	*/
	self.prototype.Abortar = function()
	{
		//Sem conexão encerra
		if(!this.Conectar()) return false;

		this.abortado = true;
		this.conexao.abort();

		cb.f.hermes.informe(this.id +'-abortado');
	}

	self.prototype.AviseMe = function(evento, func, ref, unica)
	{
		cb.f.hermes.aviseMe(func, this.id +'-'+ evento, ref, unica);
		return this;
	};

	/*
	 * Função:  Cria objeto Ajax
	 * Recebe:  Nada
	 * Retorna: BOOLEAN - True em caso de sucesso
	 * Efeito:  Altera propriedades
	*/
	self.prototype.Conectar = function()
	{
		if(this.conexao) return true;
		if(!window.XMLHttpRequest) return false;
		this.conexao = new window.XMLHttpRequest();

		if(!this.conexao){
			this.msg_id = 1;
			return false;
		}

		return true;
	};

	/*
	 * Função:  Ativa o modo de aviso pelo console das ações do objeto
	 * Recebe:  Booelano - True ativa modo global
	 * Retorna: O próprio objeto
	 * Efeito:  Altera propriedades
	*/
	self.prototype.Depurar = function(global)
	{
		if(global) __modoDepuracao = true;
		else this.modoDepuracao = true;
		return this;
	}

	/*
	 * Função:  Envia solicitação para o servidor
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Atrela eventos
	*/
	self.prototype.Enviar = function(retorno)
	{
		var self = this, dadosEnvio;

		// Atribuindo eventos de retorno
		this.AviseMe('sucesso', retorno, null, true);
		this.AviseMe('erro',    retorno, null, true);

		//Sem conexão encerra
		if(!this.Conectar())
		{
			cb.f.hermes.informe(this.id +'-erro');
			return false;
		}
		try{
			//Demais configurações
			this.conexao.addEventListener(
				'readystatechange',
				function(){ self.Resposta(); }
			);
			this.conexao.open(this.metodo, this.url , this.assinc);

			//Verificando tipo de objeto que será enviado
			if(this.dados instanceof FormData)
			{
				if('cb' in this.dados)
					dadosEnvio = this.dados.toString();
				else
				{
					dadosEnvio   = this.dados;
					this.encType = null;
				}
			} else {
				dadosEnvio = this.dados;
			}

			//Vendo EncType
			if(this.encType)
				this.conexao.setRequestHeader('Content-type', this.encType);

			//Enviando
			this.conexao.send(dadosEnvio);

		} catch(e){

			this.MsgDefinir(2);
			cb.f.hermes.informe(this.id +'-erro', this.Msg() );
		}
	}

	/*
	 * Função:  Retornar uma mensagem padronizada
	 * Recebe:  Nada
	 * Retorna: Objeto de erro
	 * Efeito:  Nada
	*/
	self.prototype.Msg = function()
	{
		var msg = this.msg_lista[this.msg_id];
		if(msg.situacao && this.formato == 'cbjson')
		{
			msg = msg.dados
		};

		// Adaptação temporária para objeto cbjson
		var obj = new cb.o.cbjson;
		obj.id       = msg.id;
		obj.situacao = msg.situacao;
		obj.tipo     = msg.tipo;
		obj.texto    = msg.texto;
		obj.dados    = msg.dados ? msg.dados : {};

		return obj;
	}

	/*
	 * Função:  Define a mensagem de erro
	 * Recebe:
	 	id    - Id do erro
	 	dados - informações para passar para os dados
	 * Retorna: Nada
	 * Efeito:  Altera propriedades
	*/
	self.prototype.MsgDefinir = function(id, dados)
	{
		this.msg_id = id;
		if(dados !== undefined) this.msg_lista[id].dados = dados;
	}

	/*
	 * Função:  Chama funções conforme situação da conexão
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Chama funções
	*/
	self.prototype.Resposta = function()
	{
		var dados;

		//Encerrar se não houver conexão
		if(!this.conexao) return false;

		switch(this.conexao.readyState){
			case 1:
				cb.f.hermes.informe(this.id +'-iniciado');
				break;
			case 4:
				this.TratarRetono();
				cb.f.hermes.informe(this.id +'-concluido');
				break;
		}
	};

	/*
	 * Função:  Informa se está em comunicação
	 * Recebe:  Nada
	 * Retorna: Booelano - True em caso positivo
	 * Efeito:  Encerra
	*/
	self.prototype.Situacao = function()
	{
		//Sem conexão encerra
		if(!this.conexao) return false;

		var sit = this.conexao.readyState;

		return (sit != 0 && sit != 4);
	};

	/*
	 * Função:  Verifica se retornou um erro ou se processou corretamente
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Chama funções
	*/
	self.prototype.TratarRetono = function()
	{
		var resposta;

		if( this.modoDepuracao || __modoDepuracao )
		{
			console.log(this.id +': '+ this.conexao.status);
			console.log(this.id +': '+ this.conexao.responseText);
		}

		if (this.conexao.status == 200 || this.conexao.status == 304)
		{
			if(this.formato == 'cbjson' || this.formato == 'json'){
				try{
					resposta = eval('('+ this.conexao.responseText +')');
				} catch(err){
					this.MsgDefinir(5, this.conexao.responseText);
					cb.f.hermes.informe(this.id +'-erro', this.Msg() );
					return false;
				}
			} else {
				resposta = this.conexao.responseText;
			}

			this.MsgDefinir(4, resposta);
			cb.f.hermes.informe(this.id +'-sucesso', this.Msg());
		}
		else
		{
			//Se tiver sido abortado
			if(this.abortado) {
				this.MsgDefinir(3, {'abortado': true});
			} else {
				this.MsgDefinir(2, {'status': this.conexao.status});
			}

			cb.f.hermes.informe(this.id +'-erro', this.Msg() );
		}
	}

	/*
	 * Função:  Ativa o modo de depuração para todos os objetos Ajax
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Chama funções
	*/
	self.Depurar = function()
	{
		__modoDepuracao = true;
		return self;
	}

	return self;

	// Lista de mensagens de erro
	function _msg(){
		return [
			{
				'id'      : 0,
				'situacao': false,
				'tipo'    : 'erro',
				'texto'   : 'Indefinido',
				'dados'   : {}
			},{
				'id'      : 1,
				'situacao': false,
				'tipo'    : 'erro',
				'texto'   : 'Erro de conexão',
				'dados'   : {}
			},{
				'id'      : 2,
				'situacao': false,
				'tipo'    : 'erro',
				'texto'   : 'Erro na comunicação com o servidor',
				'dados'   : {}
			},{
				'id'      : 3,
				'situacao': false,
				'tipo'    : 'alerta',
				'texto'   : 'Solicitação Cancelada',
				'dados'   : {}
			},{
				'id'      : 4,
				'situacao': true,
				'tipo'    : 'sucesso',
				'texto'   : 'Servidor Respondeu',
				'dados'   : {}
			},{
				'id'      : 5,
				'situacao': false,
				'tipo'    : 'erro',
				'texto'   : 'Falha ao converter os dados recebidos',
				'dados'   : {}
			}
		];
	}
})();
