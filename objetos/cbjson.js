/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

- Objeto Json
--- */
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

(function(cbl)
{
	if('cbjson' in cbl) return;

	var __contadorId = 0;

	cbl.cbjson = function()
	{
		this.id       = 0;
		this.situacao = false;
		this.tipo     = 'erro';
		this.texto    = 'Indefinido';
		this.dados    = {};
		this.objId    = 'cb.o.cbjson-'+ __contadorId++;
	}
})(cb.o);