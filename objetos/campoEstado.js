/*
==================================
			CIEBIT
==================================
Criada: 12/04/2014
Altera: 12/04/2014
----------------------------------
*/

/* ----------------------------------------
 * Fun��o a ser aplicada em campos estados para obter uma lista de cidades
*/
cb.o.estado = function(campo, alvo){
	var valor = $('option:selected', campo).attr('value');
	
	$.ajax({
		'url': '/cbpainel/geral/ajax/locais.php',
		'dataType': 'json',
		'data': {
			'acao': 'obter-cidades', 
			  'id': valor
		},
		'type': 'GET',
		'success': function(json){
			//Apagando valores atuais
			$(alvo).empty().removeAttr('disabled');
			
			//Verificando retorno
			if(json == null){
				alert('Erro Desconhecido');
			} else if(json == false){
				alert('Erro grave');
			} else {
				//Impriminido novos valores
				jQuery.each(json, function(chave, valor){
					$('<option>').attr('value', valor['id']).text(valor['nome']).appendTo(alvo);
				});
			}
		},
		'error': function(){
			alert('Houve um erro ao tentar buscar a lista de cidades, favor tente novamente');
		}
	});
};
