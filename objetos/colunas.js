/*
==================================
			CIEBIT
==================================
cb.o.colunas

Dependências:
-
----------------------------------
*/
var cb;
if (!cb) cb = {};
if (!cb.o) cb.o = {};

cb.o.colunas = (function() {
  var __contadorId = 0;

  function cboColunas(quant, area, classes) {
    //Referencia ao objeto
    this.id = 'cb-o-coluna-' + __contadorId++;
    this.area = document.querySelector(area);
    this.classes = classes;
    this.quantidade = quant;
  }

  cboColunas.prototype.CriarColunas = function() {
    for (var i = 0; i < this.quantidade; i++) {
      var col = document.createElement('div');
      col.className = 'col';
      this.area.appendChild(col);
    }
  }

  cboColunas.prototype.InserirEmColunas = function() {
    var itens = document.querySelectorAll(this.classes)
    var col = document.querySelectorAll('.col')
    for (var i = 0, x = 0; i < itens.length; i++, x++) {
      if (x == this.quantidade) {
        x = 0
      }
      col[x].appendChild(itens[i]);
    }
  }

  return cboColunas;
})();
