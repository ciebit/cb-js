/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Carrega Imagens
----------------------------------
Dependências:
- cb.f.hermes
- cb.o.cbJson
*/

(function( cb )
{
	var __contadorId = 0;

	function cboCarregaImg( caminho, responder )
	{
		// Criando propriedades
		this.objId = 'cboCarregaImg-'+ __contadorId++;
		this.url = caminho;
		this.img = new Image;

		this.carregou = false;
		this.concluido = false;
		this.erro = false;
		this.eventoErro;
		this.msg;

		// Atrelando eventos
		var $eu = this;
		this.aviseMe( 'concluido', responder );
		this.img.addEventListener( 'load', function(){ $eu._evtCarregou() } );
		this.img.addEventListener( 'error', function( e ){ $eu.eventoErro = e; $eu._evtErro() } );
		this.img.src = caminho;
	}

	/**
	* Evento que informa que carregou
	**/
	cboCarregaImg.prototype._evtCarregou = function()
	{
		// Criando resposta
		this.msg = new cb.o.cbjson;
		this.msg.situacao = true;
		this.msg.tipo = 'sucesso';
		this.msg.texto = 'Imagem carregada com sucesso';
		this.msg.dados = this.img;

		this.carregou = true;

		cb.f.hermes.avise( this.objId +'-carregou', this.msg );
		this._evtConcluio();
	}

	/**
	* Evento que informa que concluio
	**/
	cboCarregaImg.prototype._evtConcluio = function()
	{
		this.concluido = true;
		cb.f.hermes.avise( this.objId +'-concluido', this.msg );
	}

	cboCarregaImg.prototype._evtErro = function()
	{
		// Criando resposta
		this.msg = new cb.o.cbjson;
		this.msg.situacao = false;
		this.msg.tipo = 'erro';
		this.msg.texto = 'Erro ao objeter imagem';
		this.msg.dados = this.eventoErro;

		this.erro = true;

		cb.f.hermes.avise( this.objId +'-erro', this.msg );
		this._evtConcluio();
	}

	cboCarregaImg.prototype.aviseMe = function( evento, funcao )
	{
		cb.f.hermes.aviseMe( funcao, this.objId +'-'+ evento );
		return this;
	}

	cb.o.carregaImg = cboCarregaImg;

})( cb );
