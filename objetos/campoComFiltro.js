/*
==================================
------------- CIEBIT -------------
==================================
*/

cb.o.campoComFiltro = (function()
{
	var __contadorId = 0;

	/**
	* Filtra o valor de um campo
	* ----
	* @Entrada:
	* - campo - Elemento do Dom|Seletor
	* - expresao - - ExpReg - a ser aplicada para filtrar
	* - procAnt - Array  - Funções a serem aplicadas no valor do campo antes da filtragem
	* - procPos - Array - Funções a serem aplicadas no valor do campo após a filtragem
	* @Efeito:
	* @Retorno:
	**/
	function cboCampoComFiltro( campo, expressao, procAnt, procPos )
	{
		this.campo = cb.f.seletor(campo);
		this.filtro = expressao;
		this.objId = 'cb-o-campocomfiltro-'+ __contadorId++;

		this.procedimentos = {
			'anteriores': procAnt,
			'posteriores': procPos
		}

		this.situacao = {
			'instalado': false,
			'espera': false
		}

		this.tempoEspera = 500;

		this.Instalar();
	}

	/**
	* Cria um pequeno delay para iniciar os procedimentos
	* ----
	* @Efeito: Cria um evento de delay
	**/
	cboCampoComFiltro.prototype.Analizar = function()
	{
		if( this.situacao.espera ) window.clearTimeout( this.situacao.espera );

		var $eu = this;
		this.situacao.espera = window.setTimeout( function(){ $eu.Iniciar(); }, this.tempoEspera );

		return this;
	}

	/**
	* Registra solicitações de avisos do objeto
	* ----
	* @Entrada:
	* - evento - String com a identificação do aviso que deseja receber
	* - funcao - Função a ser executada
	* @Efeito:
	* @Retorno: o próprio objeto
	**/
	cboCampoComFiltro.prototype.AviseMe = function( evento, funcao )
	{
		cb.f.hermes.aviseMe( funcao, this.objId +'-'+ evento );
		return this;
	};

	/**
	* Configura o campo
	* ----
	* @Entrada: campo - Elemento do Dom
	* @Retorno: Objeto - o próprio
	**/
	cboCampoComFiltro.prototype.DefCampo = function( campo )
	{
		this.campo = cb.f.seletor(campo);
		return this;
	}

	/**
	* Configura o filtro a ser aplicado ao campo
	* ----
	* @Entrada: expresao - Expresão Regular
	* @Retorno: Objeto - o próprio
	**/
	cboCampoComFiltro.prototype.DefFiltro = function( expressao )
	{
		this.filtro = expressao;
		return this;
	}

	/**
	* Retorna o valor filtrado
	* ----
	* @Entrada: STRING - Valor
	* @Retorno: STRING - valor modificado
	**/
	cboCampoComFiltro.prototype.Filtrar = function( valor )
	{
		return valor.replace( this.filtro, '' );
	}

	/**
	* Executa todas as funções no campo
	* ----
	* @Efeito: Altera o valor do campo
	* @Retorno: Objeto - o próprio
	**/
	cboCampoComFiltro.prototype.Iniciar = function()
	{
		// Pegando posição do cursor
		var pos;
		if( this.campo.selectionStart ) pos = this.campo.selectionStart;

		this.campo.value =
			this.RodarProcedimentosPosteriores(
				this.Filtrar(
					this.RodarProcedimentosAnteriores( this.campo.value )
				)
			);

		// Posicionando cursor no mesmo local
		if( pos || pos === 0 ) this.campo.selectionStart = this.campo.selectionEnd = pos;

		cb.f.hermes.avise( this.objId +'-filtrado' );

		return this;
	}

	/**
	* Atrela eventos aos elementos
	* ----
	* @Efeito: Cria eventos
	* @Retorno: o próprio
	**/
	cboCampoComFiltro.prototype.Instalar = function()
	{
		if( this.situacao.instalado ) return;

		// Atrelando eventos
		var $eu = this;
		this.campo.addEventListener( 'keyup', function(){ $eu.Analizar(); } );

		this.situacao.instalado = true;

		return this;
	};

	/**
	* Executa os procedimentos antes de filtrar
	* ----
	* @Entrada:
	* - procedimentos - ARRAY - com funções
	* - valor - STRING
	* @Retorno: valor - STRING
	**/
	cboCampoComFiltro.prototype.RodarProcedimentos = function( procedimentos, valor )
	{
		// Se não houver procedimentos encerrar
		if( !procedimentos ) return valor;

		var novoValor = valor;

		procedimentos.forEach( function( f )
		{
			novoValor = f( novoValor );
		});

		return novoValor;
	}

	/**
	* Executa os procedimentos antes de filtrar
	* ----
	* @Entrada: valor - STRING
	* @Retorno: valor - STRING
	**/
	cboCampoComFiltro.prototype.RodarProcedimentosAnteriores = function( valor )
	{
		return this.RodarProcedimentos( this.procedimentos.anteriores, valor );
	}

	/**
	* Executa os procedimentos após a filtragem
	* ----
	* @Entrada: valor - STRING
	* @Retorno: valor - STRING
	**/
	cboCampoComFiltro.prototype.RodarProcedimentosPosteriores = function( valor )
	{
		return this.RodarProcedimentos( this.procedimentos.posteriores, valor );
	}


	/**
	* Retorna o valor do campo
	* ----
	* @Retorno: valor - STRING
	**/
	cboCampoComFiltro.prototype.Valor = function()
	{
		return this.campo.value;
	}

	return cboCampoComFiltro;
})();
