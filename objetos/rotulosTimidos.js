/*
==================================
------------- CIEBIT -------------
==================================
* cb.o.rotulosTimidos
Gerencia um campo de formulário
e seu rótulo ocultando-o quando
perde o foco.

* Dependências:
- cb.s.addEventListener
- cb.f.classes
- cb.f.seletor
----------------------------------
*/

var cb;
if(!cb) cb = {};
if(!cb.o) cb.o = {};

(function(cbo)
{
	if('rotulosTimidos' in cbo) return;

	var __contadorId = 0;

	cbo.rotulosTimidos = function cboRotulosTimidos(campo)
	{
		//Referencia ao objeto
		this.classeOculto = 'cb-rotulo_oculto';
		this.classeVisivel = 'cb-rotulo_visivel';
		this.dom = {
			'campo': cb.f.seletor(campo),
			'rotulo': undefined
		}
		this.situacao = {
			'instalado': false,
			'visivel': false
		}
		this.objId = 'cb-o-rotulosTimidos-'+ __contadorId++;

		this.Instalar();
	}

	/**
	* Seleciona o rótulo e adiciona eventos ao campo
	* ----
	* @Entrada: Nada
	* @Retorno:
	* - BOOLEANO: True em caso de instlação
	**/
	cbo.rotulosTimidos.prototype.Instalar = function()
	{
		// Se não selecionar o campo, ou
		// se não houver id válido, encerrar
		if(
			!this.dom.campo ||
			!('id' in this.dom.campo) ||
			!this.dom.campo.id
		) {
			return false;
		}

		// Obtendo o rótulo
		this.dom.rotulo = document.querySelector('label[for='+ this.dom.campo.id +']');

		// Adicionando eventos ao campo
		var _ref = this;
		this.dom.campo.addEventListener('focus', function(){ _ref.Exibir(); });
		this.dom.campo.addEventListener('blur', function(){ _ref.Ocultar(); });

		// Ocultando
		this.Ocultar();

		return this.situacao.instalado = true;
	}

	/**
	* Exibe o rótulo
	* ----
	* @Entrada: Nada
	* @Retorno:
	* - OBJETO: O próprio
	**/
	cbo.rotulosTimidos.prototype.Exibir = function()
	{
		cb.f.classeExcAdi(
			this.dom.rotulo,
			this.classeOculto,
			this.classeVisivel
		);
		this.situacao.visivel = true;
		return this;
	}

	/**
	* Oculta o rótulo
	* ----
	* @Entrada: Nada
	* @Retorno:
	* - OBJETO: O próprio
	**/
	cbo.rotulosTimidos.prototype.Ocultar = function(){
		cb.f.classeExcAdi(
			this.dom.rotulo,
			this.classeVisivel,
			this.classeOculto
		);
		this.situacao.visivel = false;
		return this;
	}
})(cb.o);
