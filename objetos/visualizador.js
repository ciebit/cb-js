/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Visualizador
----------------------------------
Dependências:
- cb.s.addEventListener
- cb.f.hermes
- cb.f.classes
*/
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

// Visualizador
cb.o.visualizador = (function()
{
	var _contador = 0;

	var f = function()
	{
		this.ref       = 'cb-visualizador-'+ _contador++;
		this.instalado = false;
		this.legenda   = false;
		this.dom       = {
			'janela'     : undefined,
			'areaImagem' : undefined,
			'areaSobre'  : undefined,
			'areaBts'    : undefined,
			'imagem'     : undefined,
			'btFechar'   : undefined,
			'btAnterior' : undefined,
			'btProximo'  : undefined,
			'titulo'     : undefined,
			'descricao'  : undefined
		};
		this.dados =
		{
			'titulo'    : undefined,
			'descricao' : undefined,
			'imgUrl'    : undefined,
			'imgAlt'    : undefined
		};
		this.eventos =
		{
			'anterior': function(){},
			'proximo' : function(){}
		};
	}

	/*
	 * Função:  Armazena solicitação
	 * Recebe:
	 	evento - STRING - Nome do evento que espera
	 	funcao - FUNÇÃO - Função que deve ser chamada
	 * Retorna: Nada
	 * Efeito:  Armazena junto ao Hermes
	*/
	f.prototype.AviseMe = function(evento, funcao)
	{
		cb.f.hermes.aviseMe(funcao, this.ref +'-'+ evento);
		return this;
	};

	/*
	 * Função:  Exibir o visualizar
	 * Recebe:
	 	- dados - Objeto - Informações sobre o produto
	 * Retorna: Nada
	 * Efeito:  Altera atribuitos de elementos
	*/
	f.prototype.Abrir = function()
	{
		cb.f.classeExc(this.dom.janela, 'cb-vis-fechado');
		cb.f.classeAdi(this.dom.janela, 'cb-vis-aberto');
	}

	/*
	 * Função:  Oculta ou exibe os botões de navegação
	 * Recebe:
	 	- sit - Booleano - True para exibir
	 * Retorna: Objeto - O próprio
	 * Efeito:  Altera a exibição dos botões
	*/
	f.prototype.BotoesNavegacao = function( sit )
	{
		this.dom.btAnterior.hidden =
		this.dom.btProximo.hidden = !sit;
		return this;
	}

	/*
	 * Função:  Define o valor da descrição
	 * Recebe:  STRING com a descrição
	 * Retorna: O Dom da descrição
	 * Efeito:  Altera o DOM
	*/
	f.prototype.DefDescricao = function(descricao)
	{
		if(!descricao) descricao = '';
		this.dom.descricao.innerHTML = descricao;
		return this;
	}

	/*
	 * Função:  Define a imagem
	 * Recebe:  URL, ALT e tamanhos da imagem
	 * Retorna: O próprio objeto
	 * Efeito:  Altera o DOM
	*/
	f.prototype.DefImagem = function(url, largura, altura, alt)
	{
		// Correção de bug quando solicita a
		// abertura da mesma imagem
		if(this.dom.imagem.getAttribute('src') == url)
		{
			cb.f.hermes.informe(self.ref +'-imagem-carregada');
			return this;
		}

		// Informando definição de imagem
		cb.f.hermes.informe(this.ref +'-imagem-solicitada');

		// Verificando valores
		if(!largura) largura = 'auto';
		if(!altura)  altura  = 'auto';
		if(!alt)     alt     = '';

		// Alterando imagem
		this.dom.imagem.setAttribute('src', url);
		this.dom.imagem.setAttribute('width', largura);
		this.dom.imagem.setAttribute('height', altura);
		this.dom.imagem.setAttribute('alt', alt);

		return this;
	}

	/*
	 * Função:  Define o valor do título
	 * Recebe:  STRING com o título
	 * Retorna: O próprio objeto
	 * Efeito:  Altera o DOM
	*/
	f.prototype.DefTitulo = function(titulo)
	{
		this.dom.titulo.innerHTML = titulo;
		return this;
	}

	/*
	 * Função:  Exibir o visualizar
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Altera atribuitos de elementos
	*/
	f.prototype.Fechar = function()
	{
		cb.f.classeExc(this.dom.janela, 'cb-vis-aberto');
		cb.f.classeAdi(this.dom.janela, 'cb-vis-fechado');
		cb.f.hermes.informe(this.ref +'-janela-fechada');
	}

	/*
	 * Função:  Criar elementos no DOM
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Altera DOM
	*/
	f.prototype.Instalar = function()
	{
		//Se já houver sido instaldo encerrar
		if(this.instalado) return;

		//Criando elementos
		this.dom = {
			'janela'     : document.createElement('div'),
			'areaImagem' : document.createElement('div'),
			'areaSobre'  : document.createElement('div'),
			'areaBts'    : document.createElement('div'),
			'imagem'     : document.createElement('img'),
			'btFechar'   : document.createElement('button'),
			'btAnterior' : document.createElement('button'),
			'btProximo'  : document.createElement('button'),
			'titulo'     : document.createElement('h1'),
			'descricao'  : document.createElement('p')
		};

		//Adicionando ID
		this.dom.janela.className     = 'cb-vis-janela';
		this.dom.areaImagem.className = 'cb-vis-area_imagem';
		this.dom.areaSobre.className  = 'cb-vis-area_sobre';
		this.dom.areaBts.className    = 'cb-vis-area_bts';
		this.dom.imagem.className     = 'cb-vis-imagem';
		this.dom.btFechar.className   = 'cb-vis-bt_fechar';
		this.dom.btAnterior.className = 'cb-vis-bt_anterior';
		this.dom.btProximo.className  = 'cb-vis-bt_proximo';
		this.dom.titulo.className     = 'cb-vis-titulo';
		this.dom.descricao.className  = 'cb-vis-descricao';

		//Adicionado Atributos
		this.dom.btFechar.title   = 'Fechar';
		this.dom.btAnterior.title = 'Imagem Anterior';
		this.dom.btProximo.title  = 'Próxima Imagem';

		//Adicionado classe e texto nos botões
		this.dom.btFechar.className   += ' cb-vis-bt';
		this.dom.btAnterior.className += ' cb-vis-bt';
		this.dom.btProximo.className  += ' cb-vis-bt';

		this.dom.btFechar.appendChild(document.createTextNode('x'));
		this.dom.btAnterior.appendChild(document.createTextNode('&lt;'));
		this.dom.btProximo.appendChild(document.createTextNode('&gt;'));

		//Inserindo elementos na Janela
		this.dom.janela.appendChild(this.dom.areaImagem);
		this.dom.janela.appendChild(this.dom.areaSobre);
		this.dom.janela.appendChild(this.dom.areaBts);

		//Inserindo elementos na Área de Imagem
		this.dom.areaImagem.appendChild(this.dom.imagem);

		//Inserindo botões
		this.dom.areaBts.appendChild(this.dom.btFechar);
		this.dom.areaBts.appendChild(this.dom.btAnterior);
		this.dom.areaBts.appendChild(this.dom.btProximo);

		//Inserindo elementos na Área Sobre
		this.dom.areaSobre.appendChild(this.dom.titulo);
		this.dom.areaSobre.appendChild(this.dom.descricao);

		//Inserindo janela na página
		window.document.body.appendChild(this.dom.janela);

		//Escondendo janela
		this.dom.janela.className += ' cb-vis-fechado';

		// Adaptando referência para as funções
		var self = this;

		// Adicionando evento para clique fora da foto
		self.dom.areaImagem.addEventListener("click", function(e){
			if(this == e.target) {
				this.parentNode.classList.remove('cb-vis-aberto');
				this.parentNode.classList.add('cb-vis-fechado');
			}
		})

		// Adicionando evento as setas do teclado e tecla esc
		document.addEventListener('keyup', function(evt){
			if (evt.keyCode == 37) self.eventos.anterior(evt, self.dom.btAnterior);
			if (evt.keyCode == 39) self.eventos.proximo(evt, self.dom.btProximo);
			if (evt.keyCode == 27) self.Fechar();
		});

		//Adicionando evento aos botões
		this.dom.btFechar.addEventListener('click', function(evt){
			evt.preventDefault();
			self.Fechar();
		});
		this.dom.btProximo.
			addEventListener('click', function(evt){
				if(self.eventos.proximo) self.eventos.proximo(evt, this);
			});
		this.dom.btAnterior.
			addEventListener('click', function(evt){
				if(self.eventos.anterior) self.eventos.anterior(evt, this);
			});

		//Adicionando evento a imagem
		this.dom.imagem.addEventListener('load', function()
		{
			// Informando carregamento da imagem
			cb.f.hermes.informe(self.ref +'-imagem-carregada');
		});

		//Informa que foi instalado
		this.instalado = true;
	}

	return f;
})();
