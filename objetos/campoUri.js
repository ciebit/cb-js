/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Campo URI
----------------------------------
Dependências:
- cb.s.addEventListener
- cb.s.Array.indexOf.js
- cb.f.strUri
*/
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

cb.o.campoUri = (function()
{
	var __contador = 0;

	var f = function(campoObs, campoApli)
	{
		this.dom = {
			'cpObservar': campoObs,
			'cpAplicar' : campoApli
		}
		this.ignorar = [35,36,37,39,46,16,17]; 
		this.objId = 'cbo-campouri-'+ __contador++;
		this.situacao = {
			'ativo': true
		}

		if(campoObs && campoApli) this.Instalar();
	}
	f.prototype.Ativar = function()
	{ 
		this.situacao.ativo = true; 
		this.Capturar();
	}
	f.prototype.Capturar = function()
	{
		this.dom.cpAplicar.value = cb.f.strUrl(this.dom.cpObservar.value);
	}
	f.prototype.Desativar = function(){ this.situacao.ativo = false; }
	f.prototype.Instalar  = function()
	{
		var self = this;

		this.dom.cpObservar.addEventListener('keyup', function(evt)
		{
			//Encerrar se as teclas não forem significativas
			if(self.ignorar.indexOf(evt.which) >= 0) return;

			//Alterando valor de url se não tiver sido modificado manualmente
			if(self.situacao.ativo) self.Capturar();
		});
		this.dom.cpAplicar.addEventListener('keyup', function(evt){
			//Encerrar se as teclas não forem significativas
			if(self.ignorar.indexOf(evt.which) >= 0) return;

			self.Desativar();
		});
	};

	return f;
})();