/*
==================================
			CIEBIT
==================================
Criada: 12/04/2014
Altera: 12/04/2014
----------------------------------
*/

/*
	Cria o efeito de encolhe e exibir conte�dos
*/
CB.Dobradica = function(_ref)
{
	var self = this;

	//Propriedades
	this.ref = $(_ref);
	this.titulo = 'cb-dobradica-titulo';
	this.conteudo = 'cb-dobradica-conteudo';
	this.situacao = 'cb-dobradica-fechada';
	this.velocidade = 400;

	/*
	 * Fun��o: Efetua a anima��o
	 * Recebe:
	 	e	- EVENT	- Evento
	 * Retorna:
	 	Nada
	 * Efeito:
	 	Altera estilo de elementos no html
	*/
	this.Agir = function(e)
	{
		var obj, classes;

		classes = e.target.getAttribute('class');

		//Verificando se o item clicado � um t�tulo
		if(typeof(classes) != 'string' || classes.search(self.titulo) < 0) return;
		
		//Animando
		obj = $(e.target);
		obj.toggleClass(self.situacao)
			.next('.'+self.conteudo)
			.slideToggle(self.velocidade)
			.toggleClass(self.situacao);

		CB.Hermes.informe('dobradica-alternada');
	};

	this.ref.on('click', this.Agir);
}