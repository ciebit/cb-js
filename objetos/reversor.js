/*
==================================
			CIEBIT
==================================
cb.o.reversor
- Gerencia um grupo de elementos do DOM e garante que
apenas um deles contém uma determinada classe.
O sistema é acionado ao clicar em um item marcado com uma
classe especificada.

Dependências:
- cb.s.addEventListener
- cb.f.classes
----------------------------------
*/

var cb;
if(!cb) cb = {};
if(!cb.o) cb.o = {};

(function(cbo)
{
	if('reversor' in cbo) return;

	var __contadorId = 0;

	cbo.reversor =  function cboReversor()
	{
		//Referencia ao objeto
		this.objId = 'cb-o-reversor-'+ __contadorId++;
		this.classeItens = 'cb-reversor_item';
		this.classeAtivado  = 'cb-reversor_ativado';
		this.classeDesativado = 'cb-reversor_desativado';
		this.itemAtivo = undefined;

		// Situação
		this.situacao = {
			'instalado': false  // Informa se foi instalado
		}

		// Elementos
		this.dom = {
			'area': undefined,
			'itens' : [],  // area a ser comprimida
		}

	}

	/*
	* Instala o objeto
	*/
	cbo.reversor.prototype.Instalar = function()
	{
		if(this.situacao.instalado) return;

		var self = this;

		this.dom.itens = document.querySelectorAll("." + this.classeItens);
		for (var i=0; i<this.dom.itens.length; i++){
			var item = this.dom.itens[i];
			cb.f.classeExcAdi(item, self.classeAtivado, self.classeDesativado);
			item.addEventListener('click', _Alternar);
		}

		this.situacao.instalado = true;

		return true;

		function _Alternar()
		{
			if(self.itemAtivo) {
				cb.f.classeExcAdi(self.itemAtivo, self.classeAtivado, self.classeDesativado)
			}
			var alvo = this;
			if (alvo == self.itemAtivo) {
				self.itemAtivo = undefined;
				return false;
			}
			self.itemAtivo = alvo;

			if (cb.f.classeTem(alvo, self.classeDesativado)) {
				cb.f.classeExcAdi(self.itemAtivo, self.classeDesativado, self.classeAtivado)
			} else {
				cb.f.classeExcAdi(self.itemAtivo, self.classeAtivado, self.classeDesativado)
			}
		}
	};

})(cb.o);
