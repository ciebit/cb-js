/*
==================================
------------- CIEBIT -------------
==================================
cb.o.clonador

Dependências:
-
----------------------------------
*/
var cb;
if (!cb) cb = {};
if (!cb.o) cb.o = {};

cb.o.clonador = (function()
{
	var __contadorId = 0;

	function cboClonador(area, btAdi)
	{
		// Registrando propriedades
		this.area = area;
		this.btAdi = btAdi;
		this.clones = [];
		this.matador = undefined;
		this.minimo = 0;
		this.modelo = area.cloneNode(true);
		this.objId = 'cb-o-clonador-'+ __contadorId++;
		this.pai = area.parentNode;
		this.total = 0;

		// Atrelando evento
		var self = this;
		this.btAdi.addEventListener(
			"click",
			function(){ self.Clonar(); }
		);
	}

	/*
	 * Função:  Armazena solicitação
	 * Recebe:
	 * - evento - STRING - Nome do evento que espera
	 * - funcao - FUNÇÃO - Função que deve ser chamada
	 * Retorna: Nada
	 * Efeito:  Armazena junto ao Hermes
	*/
	cboClonador.prototype.AviseMe = function(evento, funcao)
	{
		cb.f.hermes.aviseMe(funcao, this.objId +'-'+ evento);
		return this;
	};

	/*
	 * Verificar se existe uma cota mínima e repõe
	*/
	cboClonador.prototype.ChecarCota = function()
	{
		if(!this.minimo) return;

		var dif = this.Total() - this.minimo;
		while( dif < 0 ) {
			this.Clonar();
			dif++;
		}

		return this;
	}

	/*
	 * Cria uma cópia do modelo
	*/
	cboClonador.prototype.Clonar = function()
	{
		// Criando clone
		var clone = this.modelo.cloneNode(true);

		// Adicionado
		this.pai.appendChild(clone);
		var i = this.clones.push(clone) -1;

		// Informando referência ao marcador
		if(this.matador) {
			var matador = clone.querySelector(this.matador);
			matador.setAttribute('data-cboclonador-item', i);
		}

		// Incrementando e encerrando
		this.total++;
		cb.f.hermes.informe(this.objId +'-clonado', clone);
		return clone;
	}

	/*
	 * Remove todos os clones existentes
	*/
	cboClonador.prototype.Limpar = function(exterminar)
	{
		var clones = this.ObterClones();

		if( !this.Total() ) return this;

		clones.forEach(function(clone)
		{
			var pai = clone.parentNode;
			pai.removeChild(clone);
		});

		this.total = 0;
		this.clones = [];

		if(!exterminar) this.ChecarCota();

		return this;
	}

	/*
	 * Configura uma classe para obsevar o elemento
	 * quando clicado e remover o respectivo clone
	*/
	cboClonador.prototype.Matador = function(matador)
	{
		var _ref = this;
		this.matador = matador;
		if( this.pai ) this.pai.addEventListener('click', _remover)
		return this;

		function _remover(evt)
		{
			var alvo = evt.target;
			if( !alvo ) return;

			var matador = alvo.parentNode.querySelector(_ref.matador);
			if(matador != alvo) return;

			var id = matador.getAttribute('data-cboclonador-item');
			_ref.pai.removeChild(_ref.clones[id]);
			delete _ref.clones[id];
			_ref.total--;

			_ref.ChecarCota();
		}
	}

	/*
	 * Une o elemento informado como modelo com os clones
	*/
	cboClonador.prototype.MesclarModelo = function()
	{
		if(!this.area) return false;

		var i = this.clones.push(this.area) -1;
		this.total++;
		if(this.matador) {
			var matador = this.area.querySelector(this.matador);
			matador.setAttribute('data-cboclonador-item', i);
		}
		this.area = undefined;
		return true;
	}

	/*
	 * Configura a quantidade minima de campos visiveis
	*/
	cboClonador.prototype.Minimo = function(num)
	{
		this.minimo = num;
		return this;
	}

	/*
	 * Retorna a lista de clones
	*/
	cboClonador.prototype.ObterClones = function()
	{
		return this.clones.filter(function(){ return true; });
	}

	/*
	 * Retorna o total de clones
	*/
	cboClonador.prototype.Total = function()
	{
		return this.total;
	}

	return cboClonador;
})();
