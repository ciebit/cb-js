/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Visualizador
----------------------------------
Dependências:
- cb.s.addEventListener
- cb.f.classes
*/
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

cb.o.janela = (function()
{
	var __contador = 0;

	var f = function()
	{
		this.dom = {
			'btFechar': undefined,
			'janela'  : undefined
		};
		this.elementos = {
			'btFechar': undefined,
			'janela'  : undefined
		};
		this.objId = 'cb-janelas-'+ __contador++;
		this.situacao = {
			'instalado': false
		}
	}
	f.prototype.DefBtFechar = function(bt)
	{
		this.elementos.btFechar = bt;
		return this;
	}
	f.prototype.DefJanela = function(janela)
	{
		this.elementos.janela = janela;
		return this;
	}
	f.prototype.Instalar = function()
	{
		var self = this;

		if(this.situacao.instalado) return false;
		if(!cb.s.paginaCarregada) 
		{
			cb.f.hermes.aviseMe(
				function(){ self.Instalar(); }, 'pagina-carregada', undefined, true
			);
			return false;
		}

		// Selecionado elementos
		this.dom.janela   = document.querySelector(this.elementos.janela);
		this.dom.btFechar = document.querySelector(this.elementos.btFechar);

		// Evento de botão
		this.dom.btFechar.addEventListener(
			'click',
			function(evt){
				evt.preventDefault();
				cb.f.classeExcAdi(self.dom.janela, 'cb-ja-aberta', 'cb-ja-fechada')
			}
		);

		return this.situacao.instalado = true;
	};

	return f;
})();