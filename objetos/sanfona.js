/*
==================================
			CIEBIT
==================================
cb.o.sanfona

Dependências:
- cb.s.addEventListener
- cb.f.classes
----------------------------------
*/

var cb;
if(!cb) cb = {};
if(!cb.o) cb.o = {};

cb.o.sanfona = (function()
{
	var __contadorId = 0;

	return function cboSanfona(area, bt)
	{
		//Referencia ao objeto
		this.id = 'cb-o-sanfona-'+ __contadorId++; 

		this.classeAberta  = 'cb-sanfona_aberta';
		this.classeFechada = 'cb-sanfona_fechada';
		this.classeBtAtivado    = 'cb-sanfona-bt_ativado';
		this.classeBtDesativado = 'cb-sanfona-bt_desativado';

		// Situação
		this.situacao = {
			'aberta'   : true, // Informa se a área está visivel
			'instalado': false  // Informa se foi instalado
		}

		// Elementos
		this.dom = {
			'area' : area,  // area a ser comprimida
			'bt'   : bt,    // Botão que aciona
		}

		this.Instalar();
	}
})();

/*
 * Abre a sanfona
*/
cb.o.sanfona.prototype.Abrir = function()
{
	cb.f.classeExcAdi(
		this.dom.area, 
		this.classeFechada, 
		this.classeAberta
	);
	cb.f.classeExcAdi(
		this.dom.bt, 
		this.classeBtDesativado,
		this.classeBtAtivado
	);
	this.situacao.aberta = true;

	return this;
}

/*
 * Alterna entre aberto e fechado, se estiver aberto, fecha
 * e o inverso também ocorre
*/
cb.o.sanfona.prototype.Alternar = function()
{
	if(this.situacao.aberta) this.Fechar();
	else this.Abrir();

	return this.situacao.aberta ? 'aberta' : 'fechada';
}

/*
 * Fecha a sanfona
*/
cb.o.sanfona.prototype.Fechar = function()
{
	cb.f.classeExcAdi(
		this.dom.area, 
		this.classeAberta,
		this.classeFechada 
	);
	cb.f.classeExcAdi(
		this.dom.bt, 
		this.classeBtAtivado,
		this.classeBtDesativado 
	);
	this.situacao.aberta = false;

	return this;
}

/*
 * Instala o objeto
*/
cb.o.sanfona.prototype.Instalar = function()
{
	if(this.situacao.instalado) return;

	var self = this;

	// Adicionando evento ao botão
	this.dom.bt.addEventListener('click', function(){ self.Alternar(); });

	this.situacao.instalado = true;

	return true;
};