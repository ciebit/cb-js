/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Galeria
----------------------------------

Dependências:
- cb.s.addEventListener
- cb.f.hermes
- cb.f.classes
- cb.f.seletor
- cb.o.visualizador
- cb.o.ajax
*/
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

// Galeria
cb.o.galeria = (function(){
	var _contador = 0;

	var f = function()
	{
		//Referencia ao objeto
		this.ref = 'galeria-'+ _contador++;
		this.instalado = false;        //Informa se foi instalado
		this.dom = {                   //Elementos do DOM
			'area'       : undefined,  //Containter de Itens
			'itens'      : [],         //Lista de miniaturas
			'anterior'   : undefined,  //Item Anterior
			'selecionada': undefined,  //Item Selecionado
			'proxima'    : undefined   //Item Próximo
		};
		this.modelo = 'ampliar'; // ampliar | obter-info | obter-img
		this.classe = 'cb-gal_item';
		this.visualizador = undefined;
	}
	/*
	 * Função:  Armazena solicitação
	 * Recebe:
	 	evento - STRING - Nome do evento que espera
	 	funcao - FUNÇÃO - Função que deve ser chamada
	 * Retorna: Nada
	 * Efeito:  Armazena junto ao Hermes
	*/
	f.prototype.AviseMe = function(evento, funcao)
	{
		cb.f.hermes.aviseMe(funcao, this.ref +'-'+ evento);
		return this;
	};

	/*
	 * Função:  Define a classe de cada item da galeria
	 * Recebe:  String com o nome da classe
	 * Retorna: O própiro objeto
	 * Efeito:  Atrela propriedades
	*/
	f.prototype.DefClasse = function(classe)
	{
		this.classe = classe;
		return this;
	}

	/*
	 * Função:  Define o modo de funcionamento da galeria
	 * Recebe:  String com o modo desejado. Opções
	 *  - ampliar    - Apenas para aumentar a imagem
	 *  - obter-info - Buscar informações no servidor
	 *  - obter-img  - Obter a imagem linkada
	 * Retorna: O própiro objeto
	 * Efeito:  Atrela propriedades
	*/
	f.prototype.DefModo = function(modo)
	{
		this.modelo = modo;
		return this;
	}

	f.prototype.selecionarFilhos = function() {
		//Selecionando miniaturas
		filhos = this.dom.area.querySelectorAll('.'+ this.classe);
		// Copiando
		if(filhos){
			var tot = filhos.length;
			this.dom.itens = [];
			for(var i = 0; i < tot; i++)
				this.dom.itens.push(filhos[i]);
		}
	}

	/*
	 * Função:  Vincular evento aos Elementos Dom
	 * Recebe:  domArea - STRING/ELEMENTO refente a área da galeria
	 * Retorna: Nada
	 * Efeito:  Atrela eventos
	*/
	f.prototype.Instalar = function(domArea)
	{
		var filhos, self = this;

		//Encerrar se já houver sido instalado
		if(this.instalado) return;

		//Definindo área
		this.dom.area = cb.f.seletor(domArea);

		//Evento ao clicar
		this.dom.area.addEventListener('click', _evtClique);

		this.selecionarFilhos();

		// Instalando Visualizador
		this.visualizador = new cb.o.visualizador;

		// Adicionando evento dos botões avancar e retorceder
		this.visualizador.eventos.anterior = _evtProxAnt;
		this.visualizador.eventos.proximo  = _evtProxAnt;
		this.visualizador.Instalar();

		//Informando instalação
		this.instalado = true;

		cb.f.hermes.informe(this.ref+'-instalado');

		return true;

		/*
		 * Função:  Capturar os valores do elemento DOM e solicitar dados
		 * Recebe:  evt - Evento de Click
		 * Retorna: Nada
		 * Efeito:  Obtem dados no servidor e informa
		*/
		function _evtClique(evt)
		{
			var sit = false,
			    ref = evt.target;

			// Buscando referencia
			do{
				// Se igual a área encerrar
				if(ref == self.dom.area) return false;

				// Se houver a classe o objeto foi encontrado
				if(cb.f.ClasseTem(ref, self.classe))
				{
					sit = true;

					//Cancela direcionamento padrão
					evt.preventDefault();
					break;
				}
			}
			while(ref = ref.parentNode);

			// Se não houver encontrado encerra
			if(!sit) return false;

			//Obtendo posição do elementos
			var pos = self.dom.itens.indexOf(ref) +1;

			//Selecionando elementos
			self.SelItem(pos);

			self.TransferirParaViz();
		};

		function _evtProxAnt(evt, elem)
		{
			evt.preventDefault();

			if(cb.f.ClasseTem(elem, 'cb-vis-bt_proximo'))
			{ self.SelProxima();  }
			else
			{ self.SelAnterior(); }

			self.TransferirParaViz();
		};
	}

	/*
	 * Função:  Extrai dados o item selecionado e solicita dados
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Solicita dados do Servidor
	*/
	f.prototype.ObterDados = function()
	{
		//Selecionado elementos
		var self = this, ref = this.dom.selecionada,

		Ajax = new cb.o.ajax;
		Ajax.url     = ref.getAttribute('data-info');
		Ajax.metodo  = 'GET';
		Ajax.formato = 'cbjson';

		//Informando a solicitação dos dados
		cb.f.hermes.informe(this.ref +'-dados-solicitados');

		Ajax.Enviar(_recepcaoDados);

		function _recepcaoDados(retorno)
		{
			// Verificando se houve falhas
			if(!retorno)
			{
				cb.f.hermes.informe(self.ref +'-dados-erro');
				window.alert('Ocorreu uma falha no sistema');
				window.open(ref.href, ref.target);
				return;
			}
			if(!retorno.situacao)
			{
				cb.f.hermes.informe(self.ref +'-dados-erro');
				window.alert(retorno.texto);
				window.open(ref.href, ref.target);
				return;
			}

			// Passando informações para o visualizador
			self.visualizador.
				DefTitulo(retorno.dados.titulo).
				DefDescricao(retorno.dados.legenda).
				DefImagem(
					retorno.dados.url,
					retorno.dados.largura,
					retorno.dados.altura,
					retorno.dados.descricao
				).
				Abrir();

			// Informando recebimento dos dados
			cb.f.hermes.informe(self.ref +'-dados-recebidos', retorno.dados);
		}
	};

	/*
	 * Função:  Seleciona a miniatura de posição informada
	 * Recebe:  posicao - número da posição da imagem inicando em 1
	 * Retorna: Boolean - True se selecionar, FALSE caso não
	 * Efeito:  Altera propriedade
	*/
	f.prototype.SelItem = function(posicao)
	{
		//Encerrar se não houver posição informada
		if(!this.dom.itens[posicao -1]) return false;

		this.dom.selecionada = undefined;
		this.dom.anterior    = undefined;
		this.dom.proxima     = undefined;

		//Selecionado
		this.dom.selecionada = this.dom.itens[posicao -1];

		//Anterior
		if(this.dom.itens[posicao -2])
			this.dom.anterior = this.dom.itens[posicao -2];

		//Próximo
		if(this.dom.itens[posicao])
			this.dom.proxima = this.dom.itens[posicao];

		return true;
	};

	/*
	 * Função:  Seleciona a próxima miniatura
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Altera propriedade
	*/
	f.prototype.SelAnterior = function()
	{
		var pos = this.dom.itens.indexOf(this.dom.anterior) +1;
		return this.SelItem(pos);
	};

	/*
	 * Função:  Seleciona a próxima miniatura
	 * Recebe:  Nada
	 * Retorna: Nada
	 * Efeito:  Altera propriedade
	*/
	f.prototype.SelProxima = function()
	{
		var pos = this.dom.itens.indexOf(this.dom.proxima) +1;
		return this.SelItem(pos);
	};

	/*
	 * Função:  Passa informações para o objeto Visualizador
	 * Recebe:  Nada
	 * Retorna: o próprio objeto
	 * Efeito:
	*/
	f.prototype.TransferirParaViz = function()
	{
		var vizDom = this.visualizador.dom;

		// Verificando tipo de ação
		if(this.modelo == 'ampliar')
		{
			this.visualizador.DefImagem(
				this.dom.selecionada.src,
				this.dom.selecionada.width,
				this.dom.selecionada.height,
				this.dom.selecionada.alt
			).
			Abrir();
		}
		else if( this.modelo == 'obter-img')
		{
			var alt = this.dom.selecionada.querySelector('img').alt
			this.visualizador.DefImagem(
				this.dom.selecionada.href,
				undefined,
				undefined,
				alt
			).
			Abrir();
		}
		else
		{
			//Obtendo dados
			this.ObterDados();
		}

		// Alternando situação dos botões
		vizDom.btProximo.disabled  = !this.dom.proxima;
		vizDom.btAnterior.disabled = !this.dom.anterior;
	};

	return f;
})();
