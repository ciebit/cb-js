/*
==================================
			CIEBIT
==================================
Criada: 17/10/2015
Altera: 17/10/2015
----------------------------------
Dependências:
- s.addEventListener.js
- f.classes.js
*/
var cb;
if(!cb)   cb   = {};
if(!cb.o) cb.o = {};

cb.o.alternador = (function (argument) 
{
	var _contador = 0;
	var f = function()
	{
		this.cancelarLink = true;
		this.classe = {
			'acionado': 'cb-alternador-acionado',
			'repouso' : 'cb-alternador-repouso',
		}
		this.dom = {
			'acionador' : undefined,
			'caixa'     : undefined,
			'btFechar'  : undefined
		}
		this.ref = 'alternador-'+ _contador++;
		this.situacao = {
			'acionado' : false,
			'instalado': false
		}
	}
	
	/*
		Aciona a caixa
	*/
	f.prototype.Acionar = function() 
	{
		cb.f.ClasseExc(this.dom.caixa    , this.classe.repouso);
		cb.f.ClasseExc(this.dom.acionador, this.classe.repouso);

		cb.f.ClasseAdi(this.dom.caixa    , this.classe.acionado);
		cb.f.ClasseAdi(this.dom.acionador, this.classe.acionado);
		
		this.situacao.acionado = true;
	}
	
	/*
		Colocar a caixa em repouso
	*/
	f.prototype.Repousar = function() 
	{
		cb.f.ClasseExc(this.dom.caixa    , this.classe.acionado);
		cb.f.ClasseExc(this.dom.acionador, this.classe.acionado);

		cb.f.ClasseAdi(this.dom.caixa    , this.classe.repouso);
		cb.f.ClasseAdi(this.dom.acionador, this.classe.repouso);

		this.situacao.acionado = false;
	}

	/*
		Atrela eventos a elementos do DOM
	*/
	f.prototype.Instalar = function() 
	{
		var self = this;

		// Acionador
		this.dom.acionador.addEventListener(
			'click', 
			function(e){ 
				if(self.cancelarLink) e.preventDefault();
				self.situacao.acionado ? self.Repousar() : self.Acionar(); 
			}
		);
		// Botão Fechar
		this.dom.btFechar.addEventListener(
			'click', 
			function(){ self.Repousar(); }
		);
	};
	return f;
})();