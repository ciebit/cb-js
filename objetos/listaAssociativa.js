/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto de Lista de Associativa
----------------------------------
Dependências:
- Sem dependências
*/

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

(function(cbl)
{
	if('listaAssociativa' in cbl) return;

	var __contadorId = 0;

	cbl.listaAssociativa = function()
	{
		this.chaveId = 0;
		this.objId   = 'cboListaAssociativa-'+ __contadorId++;
		this.lista   = {};
	}

	/*
	 * Adiciona um valor a lista
	 * @ Recebe :
	 *	- STRING - Chave da lista
	 *	- MIX	 - Valor a ser atribuido
	 * @ Efeito : Armazena valor
	 * @ Retorno: True em caso de sucesso, false caso já exista a chave
	*/
	cbl.listaAssociativa.prototype.Adicionar = function(chave, valor)
	{
		if(chave in this.lista) return false;
		this.lista[chave] = valor
	};

	/*
	 * Remove todos os itens
	 * @ Recebe : Nada
	 * @ Efeito : Limpa a lista
	 * @ Retorno: O próprio objeto
	*/
	cbl.listaAssociativa.prototype.Limpar = function()
	{
		var itens = Object.keys(this.lista);
		itens.forEach(this.Remover);
		return this;
	}

	/*
	 * Retorna um elemento conforme a posição solicitada
	 * @ Recebe : número da posição, iniciando em 1
	 * @ Retorno: objeto da lista ou undefined se não existir
	*/
	cbl.listaAssociativa.prototype.ObterPorPosicao = function(pos)
	{
		var chave = Object.keys(this.lista)[pos-1];
		return this.lista[chave];
	}

	/*
	 * Retorna um elemento conforme a chave informada
	 * @ Recebe : string com a identificação da chave
	 * @ Retorno: objeto da lista ou undefined se não existir
	*/
	cbl.listaAssociativa.prototype.ObterPorChave = function(chave)
	{
		if(!(chave in this.lista)) return undefined;
		return this.lista[chave];
	}

	/*
	 * Coverte objeto em array
	 * @ Retorno: Array com lista de valores
	*/
	cbl.listaAssociativa.prototype.ParaArray = function()
	{
		var lista = [];
		for (var chave in this.lista) {
			lista.push(this.lista[chave]);
		}
		return lista;
	}

	/*
	 * Remove item
	 * @ Recebe : string indicando a referencia do objeto
	 * @ Retorno: true em caso de sucesso
	*/
	cbl.listaAssociativa.prototype.Remover = function(chave)
	{
		if(!(chave in this.lista)) return false;
		return delete this.lista[chave];
	}

	/*
	 * Retorna o toal de elementos na lista
	 * @ Recebe : nada
	 * @ Retorno: um número
	*/
	cbl.listaAssociativa.prototype.Total = function()
	{
		return Object.keys(this.lista).length;
	}
})(cb.o);
