/*
==================================
			CIEBIT
==================================
Criada  : 22/08/2014
Alterada: 24/02/2015
----------------------------------
*/
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

cb.o.iconeProcesso = (function()
{
	var __contadorId = 0;

	return function()
	{
		//Referencia ao objeto
		this.id = 'cb-f-iconeprocesso-'+ __contadorId++;
		this.intervalo = 1000;        // Intervalo entre uma imagem e outra
		this.tempo     = undefined;   // Referência controle de intervalo
		this.contador  = 0;           // Total de solicitações de animação
		this.puloX     = undefined;   // Pulo de imagem na horizontal
		this.puloY     = undefined;   // Pulo de imagem na vertical
		this.puloMinimoX = 0;         // Limite Mínimo de imagem na horizontal
		this.puloMinimoY = 0;         // Limite Minimo de imagem na vertical
		this.puloLimiteX = 0;         // Limite de Pulo de imagem na horizontal
		this.puloLimiteY = 0;         // Limite de Pulo de imagem na vertical
		this.posInicialX = undefined; // Posição inicial Horizontal
		this.posInicialY = undefined; // Posição inicial Vertical
		this.ocultar     = true;      // Se deve ser ocultado quando não houver animação

		// Situação
		this.situacao = {
			'ativo':     false, //Informa se está rodando
			'instalado': false  //Informa se foi instalado
		}

		// Elementos
		this.dom = {
			'icone'  : undefined,  //Elemento HTML
			'destino': undefined   //Onde será salvo
		}
	}
})();

/*
 * Função:  Configura objeto
 * Recebe:  Nada
 * Retorna: Nada
 * Efeito:  Adiciona elemento ao DOM
*/
cb.o.iconeProcesso.prototype.Instalar = function()
{
	// Se já houver sido instalado apenas encerrar
	if(this.situacao.instalado) return;

	//Criando elemento
	this.dom.icone = document.createElement('div');
	this.dom.icone.className     = 'cb-icone_processo';
	this.dom.icone.style.display = 'none';

	//Se não houver desino selecionar o body
	if(!this.dom.destino) this.dom.destino = document.body;

	//Inserindo elemento no destino
	this.dom.destino.appendChild(this.dom.icone);

	this.situacao.instalado = true;
};

/*
 * Função:  Altera imagem do ícone de processo
 * Recebe:  Nada
 * Retorna: Nada
 * Efeito:  Altera elemento no DOM
*/
cb.o.iconeProcesso.prototype.Animar = function()
{
	var x, y;

	if(this.puloX !== undefined)
	{
		x = this.dom.icone.style.backgroundPositionX;
		x = _calcular(x, this.puloX, this.puloMinimoX, this.puloLimiteX);
		// Alterando Elemento
		this.dom.icone.style.backgroundPositionX = x +'px';
	}

	if(this.puloY !== undefined)
	{
		y = this.dom.icone.style.backgroundPositionY;
		y = _calcular(y, this.puloY, this.puloMinimoY, this.puloLimiteY);
		// Alterando Elemento
		this.dom.icone.style.backgroundPositionY = y +'px';
	}

	function _calcular(atual, pulo, min, max)
	{
		var calc = parseFloat(atual);

		if(!calc && calc != 0) calc = 0; // Evitando NaN

		calc += pulo;

		// Verificando se é negativo
		if(max < min){
			if(calc < max || calc > min) calc = min;
		} else {
			if(calc > max || calc < min) calc = min;
		}

		return calc;
	}
}

/*
 * Função:  Exibe o ícone
 * Recebe:  Nada
 * Retorna: Nada
 * Efeito:  Alter elemento no DOM
*/
cb.o.iconeProcesso.prototype.Iniciar = function()
{
	var self = this;
	this.contador++;

	// Se já estiver animando encerrar
	if(this.situacao.ativo) return;

	// Exibindo se for o caso
	if(this.ocultar) this.dom.icone.style.display = 'block';

	this.tempo = window.setInterval(
		function(){ self.Animar(); },
		this.intervalo
	);
	this.situacao.ativo = true;
}


/*
 * Função:  Esconde o ícone
 * Recebe:  Nada
 * Retorna: Nada
 * Efeito:  Chama funções
*/
cb.o.iconeProcesso.prototype.Parar = function()
{
	// Se não estiver animando encerrar
	if(!this.situacao.ativo) return;

	this.contador--;
	if(this.contador > 0) return;

	if(this.ocultar) this.dom.icone.style.display = 'none';

	// Definindo posição inicial
	if(this.posInicialX != undefined)
		this.dom.icone.style.backgroundPositionX = this.posInicialX +'px';
	if(this.posInicialY != undefined)
		this.dom.icone.style.backgroundPositionY = this.posInicialY +'px';

	window.clearInterval(this.tempo);
	this.situacao.ativo = false;
}
