/*
==================================
------------- CIEBIT -------------
==================================
Dependências
- cb.s.addEventListener;
- cb.f.hermes;
- cb.f.animar;
- cb.f.arredondar;
- cb.f.seletor;
*/

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.o) cb.o = new Object(null);

/* ------------
    Carrosel
*/
cb.o.carrosel = (function()
{
	var __contadorId = 0;

	var f = function()
	{
		// Elementos
		this.id  = 'cb-f-carrosel-'+ __contadorId;
		this.objId  = 'cb-f-carrosel-'+ __contadorId;
		this.situacao = {
			'instalado': false
		};
		this.dom = {
			'janela'   : null,
			'grupo'    : null,
			'btEsq'     : null,
			'btEsqLocal': null, //Elemento pai da seta esquerda
			'btDir'     : null,
			'btDirLocal': null, //Elemento pai da seta direita
			'btsGrupo'     : [],
			'btsGrupoLocal': null, //Elemento pai dos botões de área
			'elementos': [],
		};

		//Parametros
		this.tamanho = 0; //Área que os elementos ocupam
		this.espaco  = 0; //Área visivel
		this.elementoIndice = 0;
		this.enchimento = false;
		this._enchimentoTamanho = 0;

		//Configurações
		this.pulo      = 10;  //Tamanho rolagem ao acionar as setas
		this.grupoPulo = 100; //Tamanho rolagem ao acionar os botões de grupo
		this.grupoVelocidade = 40000; //Velocidade do pulo de grupo
		this._relogio_ref = 0; // referencia para o tempo
		this._relogio_intervalo = 0; // Tempo para passar automático para o próximo elemento
		this._relogio_pausa = false; // Se está configurado para pausar ao pausar o mouse
		this._relogio_pausado = false; // Indica se deve pausar a passaem automática
		this.velocidade = 400;   //Velocidade do pulo simples

		//Configuração de Setas
		this.btEsqClasse = 'bt-esq'; //Classe do botão rolar a esquerda
		this.btDirClasse = 'bt-dir'; //Classe do botão rolar a direita
	};

	// Calcula os tamanhos e espaços
	f.prototype.Ajustar = function()
	{
		var calc = 0;

		//Definindo largura basedo em seus elemtnos
		this.dom.elementos.forEach(function(elem)
		{
			var tam = elem.getBoundingClientRect(),
			estilos = window.getComputedStyle(elem, null);
			calc += Math.ceil(tam.width);
			calc += (parseInt(estilos.marginLeft) + parseInt(estilos.marginRight));
		});

		//Atribuindo largura ao grupo de elemtnos
		this.dom.grupo.style.width = calc +'px';

		//Armazenando larguras
		this.tamanho = calc;
		this.espaco  = this.dom.janela.getBoundingClientRect().width;

		if( this.enchimento ) this.AjustarEnchimento();

		return this;
	}

	/**
	* Adiciona espaçamentos antes e depois do grupo do elemento
	* para que a primeira imagem e ultima fique no centro da tela
	* ----
	* @Retorno: Objeto - O próprio
	**/
	f.prototype.AjustarEnchimento = function()
	{
		this._enchimentoTamanho = (this.espaco / 2) - (this.dom.elementos[0].offsetWidth / 2);
		this.dom.grupo.style.paddingLeft =
		this.dom.grupo.style.paddingRight = this._enchimentoTamanho +'px';
		return this;
	}

	/**
	* Ativa e desativa os botões caso o carrosel
	* cheque ao extremo
	* ----
	* @Retorno: o próprio objeto
	**/
	f.prototype.AnalisarBotoesSetas = function( rolagem )
	{
		//Removendo ou adiocnado classes das setas de houver
		if( this.dom.btEsq ) this.dom.btEsq.disabled = rolagem <= 0;
		if( this.dom.btDir ) {
			this.dom.btDir.disabled = (
				rolagem >= (this.tamanho + this._enchimentoTamanho * 2 - this.espaco)
			);
		}
		return this;
	}

	/**
	* Informa eventos do objeto
	* ----
	* @Entrada:
	* - evento - String com a identificação do aviso que deseja receber
	* - funcao - Função a ser executada
	* @Efeito:
	* @Retorno: o próprio objeto
	**/
	f.prototype.Avise = function( evento )
	{
		cb.f.hermes.avise.apply( cb.f.hermes, [this.objId +'-'+ evento].concat( Array.prototype.slice.call( arguments, 1 ) ) );
		return this;
	};

	/**
	* Registra solicitações de avisos do objeto
	* ----
	* @Entrada:
	* - evento - String com a identificação do aviso que deseja receber
	* - funcao - Função a ser executada
	* @Efeito:
	* @Retorno: o próprio objeto
	**/
	f.prototype.AviseMe = function(evento, funcao)
	{
		cb.f.hermes.aviseMe( funcao, this.objId +'-'+ evento );
		return this;
	};


	//Cria os botões de Grupo
	f.prototype.CriarBtsGrupo = function()
	{
		var tot, elem, txt, self = this;

		//Se não houver local encerrar
		if(!this.dom.btsGrupoLocal) return;

		tot = Math.ceil(this.tamanho / this.espaco);

		//Criando elementos
		for(var i=1; i <= tot; i++)
		{
			//Criando elemento
			elem = document.createElement('button');
			txt  = document.createTextNode(i);

			elem.setAttribute('data-tipo', 'grupo');
			elem.setAttribute('data-posicao', i);
			elem.appendChild(txt);

			//Marcando como ativo ser for o primeiro
			if(i == 1) elem.className = 'ativo';

			//Inserindo no local
			this.dom.grupoBtsLocal.appendChild(elem);
			this.dom.btsGrupo.push(elem);
		}

		//Definindo ação ao clicar na área de botões
		this.dom.btsGrupoLocal.addEventListener(
			'click', function(evt){ self._AcaoMover(evt) }
		);
	}

	//Cria as setas se houver local definido
	f.prototype.CriarSetas = function()
	{
		var self = this;

		//Criando setas
		if(this.dom.btEsqLocal)
		{
			this.dom.btEsq = _CriarBotao(
				'Anterior',
				this.btEsqClasse,
				'seta-esq',
				this.dom.btEsqLocal
			);
		};

		if(this.dom.btDirLocal)
		{
			this.dom.btDir = _CriarBotao(
				'Próximo',
				this.btDirClasse,
				'seta-dir',
				this.dom.btDirLocal
			);
		};

		// Criando botão
		function _CriarBotao(texto, classe, tipo, local){
			var bt  = document.createElement('button'),
			    txt = document.createTextNode(texto);
			bt.className = classe;
			bt.setAttribute('data-tipo', tipo);
			bt.appendChild(txt);
			bt.addEventListener('click', function(evt){ self._AcaoMover(evt) });
			local.appendChild(bt);
			return bt;
		}
	}

	//Define valor das propriedades
	f.prototype.Definir = function(prop, val)
	{
		this[prop] = val;
		return this;
	}

	/**
	* Recebe um seletor do elemento no DOM
	* com os elementos
	* ----
	* @Retorno: Objeto - o Próprio
	**/
	f.prototype.DefElementos = function( seletor )
	{
		this.dom.elementos = cb.f.seletorLista( seletor );
		return this;
	}

	/**
	* Recebe um seletor do elemento no DOM do
	* grupo que contem os elementos
	* ----
	* @Retorno: Objeto - o Próprio
	**/
	f.prototype.DefGrupo = function( seletor )
	{
		this.dom.grupo = cb.f.seletor( seletor );
		return this;
	}

	/**
	* Recebe um seletor do elemento no DOM de janela
	* ----
	* @Retorno: Objeto - o Próprio
	**/
	f.prototype.DefJanela = function( seletor )
	{
		this.dom.janela = cb.f.seletor( seletor );
		return this;
	}

	//Define valor das propriedades
	f.prototype.Elemento = function(prop, val)
	{
		this.dom[prop] = cb.f.seletor( val );
		return this;
	}

	//Associa os eventos e cria elementos
	f.prototype.Instalar = function()
	{
		if( this.situacao.instalado ) return;

		// Calculando espaços
		this.Ajustar();

		//Criando setas
		this.CriarSetas();

		//Criando botões de grupo
		this.CriarBtsGrupo();

		// Solicitando aviso quando a janela for redimensionada
		var self = this;
		window.addEventListener('resize', function(){ self.Ajustar(); });

		this.situacao.instalado = true;
	}

	/**
	* Realiza a animaçaõ de movimento
	* ----
	* @Entrada: rolagem - posição para onde de vir
	* @Retorno: O próprio objeto
	**/
	f.prototype.Mover = function( rolagem )
	{
		//Animando
		cb.f.Animar(
			this.dom.janela,
			{'scrollLeft': rolagem},
			this.velocidade
		);

		return this;
	}

	/**
	* Rola o carrosel para direita
	* ----
	* @Entrada: distancia - INT - Tamanho do pulo
	* @Retorno: O próprio objeto
	**/
	f.prototype.MoverParaDireita = function( distancia )
	{
		var pulo = distancia ? distancia : this.pulo
		var rolagem = this.dom.janela.scrollLeft + pulo;

		this.AnalisarBotoesSetas( rolagem );

		this.Mover( rolagem );

		return this;
	}

	/**
	* Rola o carrosel para esquerda
	* ----
	* @Entrada: distancia - INT - Tamanho do pulo
	* @Retorno: O próprio objeto
	**/
	f.prototype.MoverParaEsquerda = function( distancia )
	{
		var pulo = distancia ? distancia : this.pulo
		var rolagem = this.dom.janela.scrollLeft - pulo;

		this.AnalisarBotoesSetas( rolagem );

		//Animando
		this.Mover( rolagem );

		return this;
	}

	/**
	* Move elemento para o centro
	* ----
	* @Retorno: Objeto - o próprio
	**/
	f.prototype.MoverParaCentro = function()
	{
		var indice = this.elementoIndice;
		var elem = this.dom.elementos[ indice ];

		var elementoMeio = Math.round(this.dom.elementos.length - (this.dom.elementos.length / 2) -0.1);

		elem = this.dom.elementos[elementoMeio];
		this.elementoIndice = elementoMeio;

		this.identificaCentro(elem);

		var distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

		this.Mover( distancia );
		this.Avise("carrossel-instalado");
	}


	/**
	* Move para o próximo elemento
	* ----
	* @Retorno: Objeto - o próprio
	**/
	f.prototype.MoverParaProximo = function()
	{
		this.elementoIndice++;
		var indice = this.elementoIndice,
		elem = this.dom.elementos[ indice ];


		if( !elem ) {
			elem = this.dom.elementos[0];
			this.elementoIndice = 0;
		}

		this.identificaCentro(elem);

		var distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

		this.Mover( distancia );
		this.Avise( 'movido-proximo', elem );

	}

	/**
	* Move para o elemento anterior
	* ----
	* @Retorno: Objeto - o próprio
	**/

	f.prototype.MoverParaAnterior = function()
	{
		this.elementoIndice--;
		var indice = this.elementoIndice,
		elem = this.dom.elementos[ indice ];


		var ultimoElemento = this.dom.elementos.length - 1;

		console.log(elem);

		if( !elem ) {
			elem = this.dom.elementos[ultimoElemento];
			this.elementoIndice = ultimoElemento;
		}

		this.identificaCentro(elem);

		var distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

		this.Mover( distancia );
		this.Avise( 'movido-anterior', elem );

	}

	/**
	*Adiciona classe ao elemento que está no meio
	**/

	f.prototype.identificaCentro = function(elem)
	{
		for (var i = 0; i < this.dom.elementos.length; i++) {
			this.dom.elementos[i].classList.remove("centro")
		}
		elem.classList.add("centro")
	}

	/**
	* Retorna os elementos selecionados
	* ----
	* @Retorno: ARRAY - lista de elementos
	**/
	f.prototype.ObterElementos = function()
	{
		return this.dom.elementos;
	}

	/**
	* Ativa ou Desativa a rolagem automática
	* ----
	* @Entrada: INT - Tempo e milisegundos
	* @Efeito: Desliza par ao próximo elemento
	* @Retorno: Objeto - O próprio
	**/
	f.prototype.RolagemAutomatica = function( tempo, pausa )
	{
		this._relogio_intervalo = parseInt( tempo );
		this._relogio_pausa = !!pausa;

		if( !tempo && this._relogio_ref )
		{
			window.clearInterval( this._relogio_ref );
			return this;
		}

		var $eu = this;

		// Configurando eventos para caso de pausas
		if( this._relogio_pausa )
		{
			this.dom.grupo.addEventListener( 'mouseover', function(){ $eu._relogio_pausado = true; } );
			this.dom.grupo.addEventListener( 'mouseout', function(){ $eu._relogio_pausado = false; } );
		}

		this._relogio_ref = window.setInterval( function()
		{
			if( !$eu._relogio_pausado ) $eu.MoverParaProximo();
		}, tempo );

		return this;
	}

	/**
	* Ação ao clicar em botões de seta e/ou grupo
	* ----
	* @Entrada: evt - Evento
	**/
	f.prototype._AcaoMover = function(evt)
	{
		var rolagem,
		    elem = evt.target,
		    tipo = elem.getAttribute('data-tipo');

		//Pegando posição atual
		rolagem = this.dom.janela.scrollLeft;

		//Vendo qual botão foi acionado
		if(tipo == 'grupo')
		{
			rolagem = (elem.getAttribute('data-posicao') - 1) * this.grupoPulo;

			// Removendo classe de elementos
			this.dom.btsGrupo.forEach(function(e){
				cb.f.ClasseExc(e, 'ativo');
			});

			// Adicionando no atual
			cb.f.ClasseAdi(elem, 'ativo');

			//Cancelando ação padrão do link
			evt.preventDefault();

			this.AnalizarBotoesSetas( rolagem );
			this.Mover( rolagem );
		}
		else if(tipo == 'seta-esq')
		{
			evt.preventDefault();
			return this.MoverParaEsquerda();
		}
		else if(tipo == 'seta-dir')
		{
			evt.preventDefault();
			return this.MoverParaDireita();
		};
	}

	return f;
})();
