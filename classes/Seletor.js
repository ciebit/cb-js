var Seletor = (function () {
    function Seletor() {
    }
    Seletor.item = function (elem) {
        if (elem instanceof HTMLElement)
            return elem;
        if (elem instanceof NodeList)
            return elem[0];
        // Caso deva selecionar no dom
        if ((typeof elem) == 'string')
            return;
        document.querySelector(elem);
        return null;
    };
    Seletor.lista = function (elem) {
        // Caso elementos já selecionados
        if (elem instanceof HTMLElement)
            return [elem];
        if (elem instanceof NodeList)
            return elem;
        // Caso deva selecionar no dom
        if ((typeof elem) == 'string')
            return Array.prototype.map.call(document.querySelectorAll(elem), function (e) { return e; });
        return [];
    };
    return Seletor;
}());
