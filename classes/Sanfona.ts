module Ciebit.Js
{
    export class Sanfona
    {
        private area: HTMLElement;
        private botao: HTMLElement;
        private classeAberta: string;
        private classeBotaoAbrir: string;
        private classeBotaoFechar: string;
        private classeFechada: string;
        private static contador: number = 0;
        private id: string;

        public constructor(area:HTMLElement, botao?:HTMLElement)
        {
            this.area = area;
            this.botao = botao;
            this.classeAberta = 'cb-sanfona-aberta';
            this.classeFechada = 'cb-sanfona-fechada';
            this.classeBotaoAbrir = 'cb-sanfona-botao-abrir';
            this.classeBotaoFechar = 'cb-sanfona-botao-fechar';
            this.id = 'cb-sanfona-'+ Sanfona.contador++;

            this.armarGatilho();
        }

        public abrir(): this
        {
            this.area.classList.remove(this.classeFechada);
            this.area.classList.add(this.classeAberta);

            if (this.botao) {
                this.botao.classList.remove(this.classeBotaoAbrir);
                this.botao.classList.add(this.classeBotaoFechar);
            }

            return this;
        }

        private armarGatilho(): this
        {
            this.area.classList.add(this.classeAberta);

            if (! this.botao) {
                return this;
            }

            this.botao.classList.add(this.classeBotaoFechar);

            this.botao.addEventListener('click', () => this.alternar());

            return this;
        }

        public alternar(): this
        {
            if (this.area.classList.contains(this.classeAberta)) {
                this.fechar();
            } else {
                this.abrir();
            }

            return this;
        }

        public fechar(): this
        {
            this.area.classList.remove(this.classeAberta);
            this.area.classList.add(this.classeFechada);

            if (this.botao) {
                this.botao.classList.remove(this.classeBotaoFechar);
                this.botao.classList.add(this.classeBotaoAbrir);
            }

            return this;
        }
    }
}
