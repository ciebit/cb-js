var Animar = (function () {
    function Animar(obj, param, velocidade) {
        this.prefixo = true;
        this.val = {};
        this.exp = /(\-?\d+)([\%\w\W]+)/;
        this.quaSeg = 30;
        for (var temp in param) {
            var expTemp = [];
            if (temp == 'left') {
                expTemp[1] = obj.offsetLeft;
                expTemp[2] = 'px';
            }
            else if (temp == 'scrollTop') {
                this.prefixo = false;
                expTemp[1] = obj.scrollTop;
                expTemp[2] = '';
            }
            else if (temp == 'scrollLeft') {
                this.prefixo = false;
                expTemp[1] = obj.scrollLeft;
                expTemp[2] = '';
            }
            else
                expTemp = this.exp.exec(obj.style[temp]);
            this.val[temp] = {};
            this.val[temp].prefixo = this.prefixo;
            this.val[temp].propriedade = temp;
            this.val[temp].inicio = parseInt(expTemp[1]);
            this.val[temp].decorrido = this.val[temp].inicio;
            this.val[temp].fim = param[temp];
            this.val[temp].unidade = expTemp[2];
            this.val[temp].distancia = this.val[temp].fim > this.val[temp].inicio ?
                this.val[temp].fim - this.val[temp].inicio :
                this.val[temp].inicio - this.val[temp].fim;
            this.val[temp].pulo = this.val[temp].distancia / (velocidade / 20);
            this.iniciar(obj, this.val[temp]);
        }
    }
    Animar.prototype.iniciar = function (obj, param) {
        var uma = false;
        //Executando anima��o
        var intervalo = window.setInterval(function () {
            if ((param.inicio < param.fim &&
                param.decorrido >= param.fim) ||
                (param.inicio >= param.fim &&
                    param.decorrido <= param.fim)) {
                window.clearInterval(intervalo);
                return;
            }
            if (param.fim > param.inicio) {
                param.decorrido = param.decorrido + param.pulo;
                if (param.decorrido > param.fim)
                    param.decorrido = param.fim;
            }
            else {
                param.decorrido = param.decorrido - param.pulo;
                if (param.decorrido < param.fim)
                    param.decorrido = param.fim;
            }
            var calc = param.decorrido + param.unidade;
            if (param.prefixo) {
                obj.style[param.propriedade] = calc;
            }
            else {
                obj[param.propriedade] = calc;
            }
        }, 20);
    };
    return Animar;
}());
