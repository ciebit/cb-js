/// <reference path="../../api/js/classes/Pedido.ts" />

class Api
{
	static contador:number = 0;
	private objId:number;
	private api:string;
	private pedidos:Array<Ciebit.Api.Pedido>;
	private Hermes:Ciebit.Hermes;

	constructor( api:string, pedidos:Array<Ciebit.Api.Pedido> )
	{
		this.objId = Api.contador++;
		this.api = api;
		this.pedidos = pedidos;
		this.Hermes = new Ciebit.Hermes;
	}

	/*
	* Atrela funções a eventos
	*/
	public aviseMe( evento:string, funcao:Function ):this
	{
		this.Hermes.aviseMe( evento, funcao );
		return this;
	}

	/*
	* Envia a solicitação
	*/
	public enviar( callback:Function = null )
	{
		// Criando pacote de pedidos
		var Dados = new FormData;
		Dados.append( 'pedidos', JSON.stringify( this.pedidos ) );

		// Criando objeto Ajax
		var Ajax = new XMLHttpRequest;
		Ajax.open('POST', this.api, true);
		Ajax.onreadystatechange = _resposta;
		Ajax.send(Dados);

		function _resposta()
		{
			if( this.readyState != 4 || this.status != 200 ) return;
			if( callback ) callback( JSON.parse( this.responseText ) );
		};
	}
}
