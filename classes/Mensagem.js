var Mensagem = (function () {
    function Mensagem(cod, tip, txt, dad) {
        this.codigo = cod;
        this.dados = dad;
        this.situacao = tip == 'sucesso' ? true : false;
        this.texto = txt;
        this.tipo = tip;
    }
    Mensagem.prototype.obterCodigo = function () {
        return this.codigo;
    };
    Mensagem.prototype.obterDados = function () {
        return this.dados;
    };
    Mensagem.prototype.obterSituacao = function () {
        return this.situacao;
    };
    Mensagem.prototype.obterTexto = function () {
        return this.texto;
    };
    Mensagem.prototype.obterTipo = function () {
        return this.tipo;
    };
    return Mensagem;
}());
