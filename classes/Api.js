/// <reference path="../../api/js/classes/Pedido.ts" />
var Api = (function () {
    function Api(api, pedidos) {
        this.objId = Api.contador++;
        this.api = api;
        this.pedidos = pedidos;
        this.Hermes = new Ciebit.Hermes;
    }
    /*
    * Atrela funções a eventos
    */
    Api.prototype.aviseMe = function (evento, funcao) {
        this.Hermes.aviseMe(evento, funcao);
        return this;
    };
    /*
    * Envia a solicitação
    */
    Api.prototype.enviar = function (callback) {
        if (callback === void 0) { callback = null; }
        // Criando pacote de pedidos
        var Dados = new FormData;
        Dados.append('pedidos', JSON.stringify(this.pedidos));
        // Criando objeto Ajax
        var Ajax = new XMLHttpRequest;
        Ajax.open('POST', this.api, true);
        Ajax.onreadystatechange = _resposta;
        Ajax.send(Dados);
        function _resposta() {
            if (this.readyState != 4 || this.status != 200)
                return;
            if (callback)
                callback(JSON.parse(this.responseText));
        }
        ;
    };
    return Api;
}());
Api.contador = 0;
