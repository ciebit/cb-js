/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Visualizador
----------------------------------
Dependências:
- cb.s.addEventListener
- cb.f.hermes
- cb.f.classes
*/

class Visualizador {
	private static contador:number = 0;
	private objId:string;

	private legenda:boolean;
	private instalado:boolean;

	private domJanela:HTMLElement;
	private domAreaImagem:HTMLElement;
	private domAreaSobre:HTMLElement;
	private domAreaBts:HTMLElement;
	private domImagem:HTMLImageElement;
	private domBtFechar:HTMLButtonElement;
	private domBtAnterior:HTMLButtonElement;
	private domBtProximo:HTMLButtonElement;
	private domTitulo:HTMLElement;
	private domDescricao:HTMLElement;

	private dadosTitulo:HTMLElement;
	private dadosDescricao:HTMLElement;
	private dadosImgUrl:HTMLElement;
	private dadosImgAlt:HTMLElement;

	private Hermes:Ciebit.Hermes;

	public constructor()
	{
		this.objId = 'cb-visualizador-'+ Visualizador.contador++;
		this.Hermes = new Ciebit.Hermes;
		this.legenda = false;
	}


	/*
	* Exibir o visualizar
	*/
	public abrir():this
	{
		this.domJanela.classList.remove('cb-vis-fechado');
		this.domJanela.classList.add('cb-vis-aberto');
		return this;
	}

	/*
	* Atrela eventos
	*/
	public aviseMe( evento:string, funcao:Function ):this
	{
		this.Hermes.aviseMe( evento, funcao );
		return this;
	};

	/*
	* Define o estado do botão voltar
	*/
	public btAnteriorAtivo( sit:boolean ):this
	{
		this.domBtAnterior.disabled = !sit;
		return this;
	}

	/*
	* Define o estado do botão avançar
	*/
	public btProximoAtivo( sit:boolean ):this
	{
		this.domBtProximo.disabled = !sit;
		return this;
	}

	/*
	* Define a visibilidade dos botões de navegação
	*/
	public botoesNavegacao( sit:boolean ):this
	{
		this.domBtAnterior.hidden =
		this.domBtProximo.hidden = !sit;
		return this;
	}

	/*
	* Função:  Define a imagem
	*/
	public defImagem( url:string, largura: string|number = 'auto', altura:string|number = 'auto', alt:string = '' ):this
	{
		// Correção de bug quando solicita a
		// abertura da mesma imagem
		if(this.domImagem.getAttribute('src') == url)
		{
			this.Hermes.avise( 'imagem-carregada', url );
			return this;
		}

		// Informando definição de imagem
		this.Hermes.avise( 'imagem-solicitada', url );

		// Alterando imagem
		this.domImagem.setAttribute('src', url);
		this.domImagem.setAttribute('width', largura.toString() );
		this.domImagem.setAttribute('height', altura.toString() );
		this.domImagem.setAttribute('alt', alt);

		return this;
	}

	/*
	 * Função:  Define o valor da descrição
	 * Recebe:  STRING com a descrição
	 * Retorna: O Dom da descrição
	 * Efeito:  Altera o DOM
	*/
	set descricao (descricao)
	{
		if(!descricao) descricao = '';
		this.domDescricao.innerHTML = descricao;
	}

	/*
	 * Oculta o visualizar
	*/
	public fechar():this
	{
		this.domJanela.classList.remove('cb-vis-aberto');
		this.domJanela.classList.add('cb-vis-fechado');
		this.Hermes.avise( 'janela-fechada' );
		return this;
	}

	/*
	 * Criar elementos no DOM e atrela eventos
	*/
	public instalar():this
	{
		//Se já houver sido instaldo encerrar
		if( this.instalado ) return this;

		//Criando elementos
		this.domJanela     = document.createElement('div');
		this.domAreaImagem = document.createElement('div');
		this.domAreaSobre  = document.createElement('div');
		this.domAreaBts    = document.createElement('div');
		this.domImagem     = document.createElement('img');
		this.domBtFechar   = document.createElement('button');
		this.domBtAnterior = document.createElement('button');
		this.domBtProximo  = document.createElement('button');
		this.domTitulo     = document.createElement('h1');
		this.domDescricao  = document.createElement('p');


		//Adicionando ID
		this.domJanela.className     = 'cb-vis-janela';
		this.domAreaImagem.className = 'cb-vis-area_imagem';
		this.domAreaSobre.className  = 'cb-vis-area_sobre';
		this.domAreaBts.className    = 'cb-vis-area_bts';
		this.domImagem.className     = 'cb-vis-imagem';
		this.domBtFechar.className   = 'cb-vis-bt_fechar';
		this.domBtAnterior.className = 'cb-vis-bt_anterior';
		this.domBtProximo.className  = 'cb-vis-bt_proximo';
		this.domTitulo.className     = 'cb-vis-titulo';
		this.domDescricao.className  = 'cb-vis-descricao';

		//Adicionado Atributos
		this.domBtFechar.title   = 'Fechar';
		this.domBtAnterior.title = 'Imagem Anterior';
		this.domBtProximo.title  = 'Próxima Imagem';

		//Adicionado classe e texto nos botões
		this.domBtFechar.className   += ' cb-vis-bt';
		this.domBtAnterior.className += ' cb-vis-bt';
		this.domBtProximo.className  += ' cb-vis-bt';

		this.domBtFechar.appendChild(document.createTextNode('x'));
		this.domBtAnterior.appendChild(document.createTextNode('<'));
		this.domBtProximo.appendChild(document.createTextNode('>'));

		//Inserindo elementos na Janela
		this.domJanela.appendChild(this.domAreaImagem);
		this.domJanela.appendChild(this.domAreaSobre);
		this.domJanela.appendChild(this.domAreaBts);

		//Inserindo elementos na Área de Imagem
		this.domAreaImagem.appendChild(this.domImagem);

		//Inserindo botões
		this.domAreaBts.appendChild(this.domBtFechar);
		this.domAreaBts.appendChild(this.domBtAnterior);
		this.domAreaBts.appendChild(this.domBtProximo);

		//Inserindo elementos na Área Sobre
		this.domAreaSobre.appendChild(this.domTitulo);
		this.domAreaSobre.appendChild(this.domDescricao);

		//Inserindo janela na página
		window.document.body.appendChild(this.domJanela);

		//Escondendo janela
		this.domJanela.classList.add( 'cb-vis-fechado' );

		// Adicionando evento para clique fora da foto
		this.domAreaImagem.addEventListener( 'click', ( e ) => {
			if( this.domAreaImagem == e.target ) {
				this.domJanela.classList.remove( 'cb-vis-aberto' );
				this.domJanela.classList.add( 'cb-vis-fechado' );
			}
		});

		// Adicionando evento as setas do teclado e tecla esc
		document.addEventListener('keyup', ( evt ) => {
			if (evt.keyCode == 37) this.Hermes.avise( 'anterior' );
			if (evt.keyCode == 39) this.Hermes.avise( 'proximo' );
			if (evt.keyCode == 27) this.fechar();
		});

		//Adicionando evento aos botões
		this.domBtFechar.addEventListener('click', (evt) => {
			evt.preventDefault();
			this.fechar();
		});
		this.domBtProximo.addEventListener('click', (evt) => {
			this.Hermes.avise( 'proximo' );
		});
		this.domBtAnterior.addEventListener('click', (evt) => {
			this.Hermes.avise( 'anterior' );
		});

		//Adicionando evento a imagem
		// Informando carregamento da imagem
		this.domImagem.addEventListener('load', () => {
			this.Hermes.avise( 'imagem-carregada' );
		});

		//Informa que foi instalado
		this.instalado = true;

		return this;
	}

	/*
	 * Função:  Define o valor do título
	 * Recebe:  STRING com o título
	 * Retorna: O próprio objeto
	 * Efeito:  Altera o DOM
	*/
	set titulo (titulo)
	{
		this.domTitulo.innerHTML = titulo;
	}
}
