/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Galeria
----------------------------------

Dependências:
- cb.s.addEventListener
- cb.f.hermes
- cb.f.classes
- cb.f.seletor
- cb.o.visualizador
- cb.o.ajax
*/
// Galeria
var Galeria = (function () {
    function Galeria() {
    }
    /*
    * Configura o objeto
    */
    Galeria.prototype.construct = function () {
        this.objId = 'cb-galeria-' + Galeria.contador++;
        this.elemClasse = 'cb-gal_item';
        this.instalado = false;
        this.Hermes = new Ciebit.Hermes;
        this.Visualizador = new Visualizador;
    };
    /*
    * Atrela funções a eventos
    */
    Galeria.prototype.aviseMe = function (evento, funcao) {
        this.Hermes.aviseMe(evento, funcao);
        return this;
    };
    Object.defineProperty(Galeria.prototype, "classe", {
        /*
        * Método:  Define a classe de cada item da galeria
        * ----
        * @recebe:  String com o nome da classe
        * @retorna: O própiro objeto
        * @efeito:  Atrela propriedades
        */
        set: function (classe) {
            this.elemClasse = classe;
        },
        enumerable: true,
        configurable: true
    });
    /*
    * Define o caminho da api
    */
    Galeria.prototype.defApi = function (url) {
        this.api = url;
        return this;
    };
    /*
    * Verifica eventos ocorrido na área da galeria
    */
    Galeria.prototype.evtClique = function (evt) {
        var sit = false, ref = evt.target;
        // Buscando referencia
        do {
            // Se igual a área encerrar
            if (ref == this.domArea)
                return;
            // Se houver a classe o objeto foi encontrado
            if (ref.classList.contains(this.elemClasse)) {
                sit = true;
                //Cancela direcionamento padrão
                evt.preventDefault();
                break;
            }
        } while (ref = ref.parentNode);
        // Se não houver encontrado encerra
        if (!sit)
            return;
        // Se não for um link encerrar
        if (!(ref instanceof HTMLLinkElement))
            return;
        // Pegando posição do item
        var pos = this.domItens.indexOf(ref) + 1;
        //Selecionando elementos
        this.selItem(pos);
        // Tranferindo par ao visualizador
        this.transferirParaViz();
    };
    /*
    * Função:  Vincular evento aos Elementos Dom
    * ----
    * @recebe:  domArea - STRING/ELEMENTO refente a área da galeria
    * @retorna: Nada
    * @efeito:  Atrela eventos
    */
    Galeria.prototype.instalar = function (domArea) {
        var _this = this;
        //Encerrar se já houver sido instalado
        if (this.instalado)
            return;
        //Definindo área
        this.domArea = domArea;
        //Evento ao clicar
        this.domArea.addEventListener('click', function (e) { _this.evtClique(e); });
        this.selecionarFilhos();
        // Adicionando evento dos botões avancar e retorceder
        this.Visualizador.aviseMe('anterior', function (e) {
            e.preventDefault();
            _this.selProxima();
            _this.transferirParaViz();
        }).aviseMe('proximo', function (e) {
            e.preventDefault();
            _this.selAnterior();
            _this.transferirParaViz();
        }).instalar();
        //Informando instalação
        this.instalado = true;
        this.Hermes.avise('instalado');
        return this;
    };
    /*
     * Busca no servidor dados da imagem solicitada
    */
    Galeria.prototype.obterDados = function (id) {
        var _this = this;
        var PedDados = new Ciebit.Api.Pedido('obter-dados');
        PedDados.paramAdi('id', id);
        this.Api = new Api(this.api, [PedDados]);
        this.Api.enviar(function (dados) { return _this.receberDados(dados); });
        //Informando a solicitação dos dados
        this.Hermes.avise('dados-solicitados');
    };
    ;
    /*
     * Trato o valor recebido
    */
    Galeria.prototype.receberDados = function (Msg) {
        // Verificando se houve falhas
        if (!Msg.situacao()) {
            this.Hermes.avise('obter-dados-erro');
            window.open(this.domSelecionada.href, this.domSelecionada.target);
            return;
        }
        // Passando informações para o visualizador
        this.Visualizador.titulo = Msg.dados.titulo;
        this.Visualizador.descricao = Msg.dados.legenda;
        this.Visualizador.defImagem(Msg.dados.url, Msg.dados.largura, Msg.dados.altura, Msg.dados.descricao).
            abrir();
        // Informando recebimento dos dados
        this.Hermes.avise('dados-recebidos', Msg.dados);
    };
    /*
    * Seleciona o elemento anterior ao atual
    */
    Galeria.prototype.selAnterior = function () {
        var pos = this.domItens.indexOf(this.domAnterior) + 1;
        return this.selItem(pos);
    };
    ;
    /*
    * Seleciona elementos com a classe especificada
    */
    Galeria.prototype.selecionarFilhos = function () {
        var filhos = this.domArea.querySelectorAll('.' + this.classe);
        // Se não houver filhos encerrar
        if (!filhos)
            return;
        // Contando o total de elementos
        var tot = filhos.length;
        // Verificando tipo de elementos
        this.domItens = [];
        for (var i = 0; i < tot; i++)
            this.domItens.push(filhos[i]);
        return this;
    };
    /*
    * Marca um elemento da galeria como o atual
    */
    Galeria.prototype.selItem = function (posicao) {
        //Encerrar se não houver posição informada
        if (!this.domItens[posicao - 1])
            return false;
        this.domSelecionada = undefined;
        this.domAnterior = undefined;
        this.domProxima = undefined;
        //Selecionado
        this.domSelecionada = this.domItens[posicao - 1];
        //Anterior
        if (this.domItens[posicao - 2])
            this.domAnterior = this.domItens[posicao - 2];
        //Próximo
        if (this.domItens[posicao])
            this.domProxima = this.domItens[posicao];
        return true;
    };
    ;
    /*
    * Seleciona o próximo elemento da galeria
    */
    Galeria.prototype.selProxima = function () {
        var pos = this.domItens.indexOf(this.domProxima) + 1;
        return this.selItem(pos);
    };
    ;
    /*
    * Passa informações para o objeto Visualizador
    */
    Galeria.prototype.transferirParaViz = function () {
        // Verificando tipo de ação
        if (this.domSelecionada instanceof HTMLImageElement) {
            this.Visualizador.
                defImagem(this.domSelecionada.src, this.domSelecionada.width, this.domSelecionada.height, this.domSelecionada.alt).
                abrir();
        }
        else if (this.domSelecionada instanceof HTMLLinkElement) {
            // Verificando se há dados a serem coletados
            var id = this.domSelecionada.getAttribute('data-id');
            if (id) {
                this.obterDados(parseInt(id));
                return true;
            }
            // Exibindo imagem no href
            var alt = this.domSelecionada.querySelector('img').alt;
            this.Visualizador.
                defImagem(this.domSelecionada.href, undefined, undefined, alt).
                abrir();
        }
        else
            return false;
        // Alternando situação dos botões
        this.Visualizador.btProximoAtivo(!this.domProxima);
        this.Visualizador.btAnteriorAtivo(!this.domAnterior);
        return true;
    };
    ;
    return Galeria;
}());
