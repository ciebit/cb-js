/// <reference path="../Acionador.ts" />
/// <reference path="../Alvo.ts" />
console.log('arquivo chamado');
var Ciebit;
(function (Ciebit) {
    var Acionador;
    (function (Acionador_1) {
        var Controladores;
        (function (Controladores) {
            var AutoExecuta = (function () {
                function AutoExecuta(document) {
                    var _this = this;
                    if (AutoExecuta.construido) {
                        return;
                    }
                    AutoExecuta.acionadores = [];
                    AutoExecuta.seletorAcionadores = '.cb-js-acionador';
                    document.addEventListener('DOMContentLoaded', function () { return _this.ligar(); });
                    AutoExecuta.construido = true;
                    return this;
                }
                AutoExecuta.prototype.ligar = function () {
                    var acionadoresElementos = document.querySelectorAll(AutoExecuta.seletorAcionadores);
                    for (var i = 0; i < acionadoresElementos.length; i++) {
                        var AlvoElemento = document.querySelector(acionadoresElementos[i].getAttribute('data-alvo'));
                        if (!AlvoElemento) {
                            continue;
                        }
                        var Alvo_1 = new Ciebit.Acionador.Alvo(AlvoElemento);
                        var Acionador_2 = new Ciebit.Acionador.Acionador(acionadoresElementos[i], Alvo_1);
                        AutoExecuta.acionadores.push(Acionador_2);
                    }
                    return this;
                };
                return AutoExecuta;
            }());
            Controladores.AutoExecuta = AutoExecuta;
        })(Controladores = Acionador_1.Controladores || (Acionador_1.Controladores = {}));
    })(Acionador = Ciebit.Acionador || (Ciebit.Acionador = {}));
})(Ciebit || (Ciebit = {}));
new Ciebit.Acionador.Controladores.AutoExecuta(document);
