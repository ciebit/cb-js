/// <reference path="../Acionador.ts" />
/// <reference path="../Alvo.ts" />

console.log('arquivo chamado');

module Ciebit.Acionador.Controladores
{
    export class AutoExecuta
    {
        private static acionadores: Array<Ciebit.Acionador.Acionador>;
        private static construido: boolean;
        private static seletorAcionadores: string;

        public constructor(document: Document)
        {
            if (AutoExecuta.construido) {
                return;
            }

            AutoExecuta.acionadores = [];
            AutoExecuta.seletorAcionadores = '.cb-js-acionador';

            document.addEventListener('DOMContentLoaded', () => this.ligar());

            AutoExecuta.construido = true;

            return this;
        }

        private ligar(): AutoExecuta
        {
            let acionadoresElementos = <NodeListOf<HTMLElement>>document.querySelectorAll(AutoExecuta.seletorAcionadores);

            for (let i = 0; i < acionadoresElementos.length; i++) {
                let AlvoElemento = <HTMLElement>document.querySelector(acionadoresElementos[i].getAttribute('data-alvo'));

                if (! AlvoElemento) {
                    continue;
                }

                let Alvo = new Ciebit.Acionador.Alvo(AlvoElemento);
                let Acionador = new Ciebit.Acionador.Acionador(acionadoresElementos[i], Alvo);

                AutoExecuta.acionadores.push(Acionador);
            }

            return this;
        }
    }
}

new Ciebit.Acionador.Controladores.AutoExecuta(document);
