/// <reference path="Alvo.ts" />
var Ciebit;
(function (Ciebit) {
    var Acionador;
    (function (Acionador_1) {
        var Acionador = (function () {
            function Acionador(Elemento, alvo) {
                this.Alvo = alvo;
                this.classeAtivo = 'cb-js-acionador-acionador-ativo';
                this.classeInativo = 'cb-js-acionador-acionador-inativo';
                this.Elemento = Elemento;
                this.atrelarEvento();
                if (!this.Elemento.classList.contains(this.classeAtivo)) {
                    this.inativar();
                }
            }
            Acionador.prototype.alternar = function () {
                this.obterSituacao() ? this.inativar() : this.ativar();
                return this;
            };
            Acionador.prototype.atrelarEvento = function () {
                var _this = this;
                this.Elemento.addEventListener('click', function () { return _this.alternar(); });
                return this;
            };
            Acionador.prototype.ativar = function () {
                this.Elemento.classList.remove(this.classeInativo);
                this.Elemento.classList.add(this.classeAtivo);
                this.Alvo.ativar();
                return this;
            };
            Acionador.prototype.definirClasseAtivo = function (classe) {
                this.classeAtivo = classe;
                return this;
            };
            Acionador.prototype.definirClasseInativo = function (classe) {
                this.classeInativo = classe;
                return this;
            };
            Acionador.prototype.inativar = function () {
                this.Elemento.classList.remove(this.classeAtivo);
                this.Elemento.classList.add(this.classeInativo);
                this.Alvo.inativar();
                return this;
            };
            Acionador.prototype.obterSituacao = function () {
                return this.Elemento.classList.contains(this.classeAtivo);
            };
            return Acionador;
        }());
        Acionador_1.Acionador = Acionador;
    })(Acionador = Ciebit.Acionador || (Ciebit.Acionador = {}));
})(Ciebit || (Ciebit = {}));
