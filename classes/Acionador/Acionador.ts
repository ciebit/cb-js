/// <reference path="Alvo.ts" />

module Ciebit.Acionador
{
    export class Acionador
    {
        private Alvo: Ciebit.Acionador.Alvo;
        private classeAtivo: string;
        private classeInativo: string;
        private Elemento: HTMLElement;

        public constructor(Elemento:HTMLElement, alvo: Ciebit.Acionador.Alvo)
        {
            this.Alvo = alvo;
            this.classeAtivo = 'cb-js-acionador-acionador-ativo';
            this.classeInativo = 'cb-js-acionador-acionador-inativo';
            this.Elemento = Elemento;

            this.atrelarEvento();

            if (! this.Elemento.classList.contains(this.classeAtivo)) {
                this.inativar();
            }
        }

        public alternar(): this
        {
            this.obterSituacao() ? this.inativar() : this.ativar();

            return this;
        }

        private atrelarEvento(): this
        {
            this.Elemento.addEventListener('click', () => this.alternar());
            return this;
        }

        public ativar(): this
        {
            this.Elemento.classList.remove(this.classeInativo);
            this.Elemento.classList.add(this.classeAtivo);

            this.Alvo.ativar();

            return this;
        }

        public definirClasseAtivo(classe: string): this
        {
            this.classeAtivo = classe;
            return this;
        }

        public definirClasseInativo(classe: string): this
        {
            this.classeInativo = classe;
            return this;
        }

        public inativar(): this
        {
            this.Elemento.classList.remove(this.classeAtivo);
            this.Elemento.classList.add(this.classeInativo);

            this.Alvo.inativar();

            return this;
        }

        public obterSituacao(): boolean
        {
            return this.Elemento.classList.contains(this.classeAtivo);
        }
    }
}
