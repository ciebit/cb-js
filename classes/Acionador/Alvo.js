var Ciebit;
(function (Ciebit) {
    var Acionador;
    (function (Acionador) {
        var Alvo = (function () {
            function Alvo(Elemento) {
                this.Elemento = Elemento;
                this.classeAtivo = 'cb-js-acionador-alvo-ativo';
                this.classeInativo = 'cb-js-acionador-alvo-inativo';
                if (!this.Elemento.classList.contains(this.classeAtivo)) {
                    this.inativar();
                }
            }
            Alvo.prototype.ativar = function () {
                this.Elemento.classList.remove(this.classeInativo);
                this.Elemento.classList.add(this.classeAtivo);
                return this;
            };
            Alvo.prototype.definirClasseAtivo = function (classe) {
                this.classeAtivo = classe;
                return this;
            };
            Alvo.prototype.definirClasseInativo = function (classe) {
                this.classeInativo = classe;
                return this;
            };
            Alvo.prototype.inativar = function () {
                this.Elemento.classList.remove(this.classeAtivo);
                this.Elemento.classList.add(this.classeInativo);
                return this;
            };
            return Alvo;
        }());
        Acionador.Alvo = Alvo;
    })(Acionador = Ciebit.Acionador || (Ciebit.Acionador = {}));
})(Ciebit || (Ciebit = {}));
