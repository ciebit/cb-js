module Ciebit.Acionador
{
    export class Alvo
    {
        private classeAtivo: string;
        private classeInativo: string;
        private Elemento: HTMLElement;

        public constructor(Elemento:HTMLElement)
        {
            this.Elemento = Elemento;

            this.classeAtivo = 'cb-js-acionador-alvo-ativo';
            this.classeInativo = 'cb-js-acionador-alvo-inativo';

            if (! this.Elemento.classList.contains(this.classeAtivo)) {
                this.inativar();
            }
        }

        public ativar(): this
        {
            this.Elemento.classList.remove(this.classeInativo);
            this.Elemento.classList.add(this.classeAtivo);
            return this;
        }

        public definirClasseAtivo(classe: string): this
        {
            this.classeAtivo = classe;
            return this;
        }

        public definirClasseInativo(classe: string): this
        {
            this.classeInativo = classe;
            return this;
        }

        public inativar(): this
        {
            this.Elemento.classList.remove(this.classeAtivo);
            this.Elemento.classList.add(this.classeInativo);
            return this;
        }
    }
}
