var Pedido = (function () {
    function Pedido(acao) {
        this.id = Pedido.contador++;
        this.acao = acao;
        this.param = {};
    }
    Pedido.prototype.paramAdi = function (parametro, valor) {
        this.param[parametro] = valor;
        return this;
    };
    return Pedido;
}());
Pedido.contador = 0;
