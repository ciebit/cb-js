class Mensagem
{
	public codigo:string;
	public dados;
	public situacao:boolean;
	public texto:string;
	public tipo:string;

	constructor( cod:string, tip:string, txt:string, dad )
	{
		this.codigo = cod;
		this.dados = dad;
		this.situacao = tip == 'sucesso' ? true : false;
		this.texto = txt;
		this.tipo = tip;
	}

	public obterCodigo():string
	{
		return this.codigo;
	}

	public obterDados()
	{
		return this.dados;
	}

	public obterSituacao():boolean
	{
		return this.situacao;
	}

	public obterTexto():string
	{
		return this.texto;
	}

	public obterTipo():string
	{
		return this.tipo;
	}
}
