class Carrossel {

  id: string;
  objId: string;
  situacaoInstalado: boolean;
  _contadorId: number;
  domJanela: HTMLElement;
  domGrupo: HTMLElement;
  domBtEsq: HTMLButtonElement;
  domBtEsqLocal: HTMLElement;
  domBtDir: HTMLButtonElement;
  domBtDirLocal: HTMLElement;
  domBtsGrupo: Array<HTMLElement>;
  domBtsGrupoLocal: HTMLElement;
  domElementos: Array<HTMLElement>;

  tamanho: number;
  espaco: number;
  elementoIndice: number;
  enchimento: boolean;
  _enchimentoTamanho: number;

  pulo: number;
  grupoPulo: number;
  grupoVelocidade: number;
  _relogio_ref: number;
  _relogio_intervalo: number;
  _relogio_pausa: boolean;
  _relogio_pausado: boolean;
  velocidade: number;
  btEsqClasse: string;
  btDirClasse: string;

  Hermes: Ciebit.Hermes;
  Animar: Animar;

  constructor() {
    this.Hermes = new Ciebit.Hermes;
    this.id = 'cb-carrossel-' + this._contadorId;
    this.objId = 'cb-carrossel-' + this._contadorId;
    this.situacaoInstalado = false;

    // Parâmetros
    this.tamanho = 0; //Área que os elementos ocupam
		this.espaco  = 0; //Área visivel
		this.elementoIndice = 0;
		this.enchimento = false;
		this._enchimentoTamanho = 0;

    // Configurações
    this.pulo      = 10;  //Tamanho rolagem ao acionar as setas
		this.grupoPulo = 100; //Tamanho rolagem ao acionar os botões de grupo
		this.grupoVelocidade = 40000; //Velocidade do pulo de grupo
		this._relogio_ref = 0; // referencia para o tempo
		this._relogio_intervalo = 0; // Tempo para passar automático para o próximo elemento
		this._relogio_pausa = false; // Se está configurado para pausar ao pausar o mouse
		this._relogio_pausado = false; // Indica se deve pausar a passaem automática
		this.velocidade = 400;   //Velocidade do pulo simples

		//Configuração de Setas
		this.btEsqClasse = 'bt-esq'; //Classe do botão rolar a esquerda
		this.btDirClasse = 'bt-dir'; //Classe do botão rolar a direita
  }

  //Calcula os tamanhos e espaços
  ajustar() {
    let calc = 0;

    //Definindo largura basedo em seus elemtnos
    this.domElementos.forEach(function(elem){
      let tam = elem.getBoundingClientRect(),
      estilos = window.getComputedStyle(elem, null);
      calc += (parseInt(estilos.marginLeft) +
      parseInt(estilos.marginRight));
    })

    //Atribuindo largura ao grupo de elemtnos
    this.domGrupo.style.width = calc + "px";

    //Armazenando larguras
    this.tamanho = calc;
    this.espaco = this.domJanela.getBoundingClientRect().width;

    if (this.enchimento) this.ajustarEnchimento();

    return this;
  }

  /**
  * Adiciona espaçamentos antes e depois do grupo do elemento
  * para que a primeira imagem e ultima fique no centro da tela
  * ----
  * @Retorno: Objeto - O próprio
  **/
  ajustarEnchimento() {
    this._enchimentoTamanho = (this.espaco / 2) -
    (this.domElementos[0].offsetWidth / 2);
    this.domGrupo.style.paddingLeft =
    this.domGrupo.style.paddingRight = this._enchimentoTamanho + "px";
    return this;
  }

  /**
  * Ativa e desativa os botões caso o carrosel
  * cheque ao extremo
  * ----
  * @Retorno: o próprio objeto
  **/
  analisarBotoesSetas(rolagem) {
    //Removendo ou adiocnado classes das setas de houver
    if (this.domBtEsq) this.domBtEsq.disabled = rolagem <= 0;
    if (this.domBtDir) {
        this.domBtDir.disabled = (rolagem >= (this.tamanho + this._enchimentoTamanho * 2 - this.espaco));
    }
    return this;
  }

  /**
	* Informa eventos do objeto
	* ----
	* @Entrada:
	* - evento - String com a identificação do aviso que deseja receber
	* - funcao - Função a ser executada
	* @Efeito:
	* @Retorno: o próprio objeto
	**/

  avise(evento, args?) {
    this.Hermes.avise.apply(this.Hermes, [this.objId + '-' + evento].concat(Array.prototype.slice.call(arguments, 1)));
    return this;
  }

  /**
  * Registra solicitações de avisos do objeto
  * ----
  * @Entrada:
  * - evento - String com a identificação do aviso que deseja receber
  * - funcao - Função a ser executada
  * @Efeito:
  * @Retorno: o próprio objeto
  **/
  aviseMe(evento:string, funcao:Function) {
    this.Hermes.aviseMe(this.objId + '-' + evento, funcao);
    return this;
  }

  //Cria os botões de Grupo
  criarBtsGrupo() {
    let tot, elem, txt, self = this;

    //Se não houver local encerrar
		if(!this.domBtsGrupoLocal) return;

    tot = Math.ceil(this.tamanho / this.espaco);

		//Criando elementos
    for(let i=1; i <= tot; i++)
		{
			//Criando elemento
			elem = document.createElement('button');
			txt  = document.createTextNode(String(i));

			elem.setAttribute('data-tipo', 'grupo');
			elem.setAttribute('data-posicao', i);
			elem.appendChild(txt);

			//Marcando como ativo ser for o primeiro
			if(i == 1) elem.className = 'ativo';

			//Inserindo no local
			this.domBtsGrupoLocal.appendChild(elem);
			this.domBtsGrupo.push(elem);
		}

    //Definindo ação ao clicar na área de botões
		this.domBtsGrupoLocal.addEventListener(
			'click', function(evt){ self.acaoMover(evt) }
		);
  }

  //Cria as setas se houver local definido
  criarSetas() {

		//Criando setas
		if(this.domBtEsqLocal)
		{
			this.domBtEsq = this.criarBotao(
				'Anterior',
				this.btEsqClasse,
				'seta-esq',
				this.domBtEsqLocal
			);
		};

		if(this.domBtDirLocal)
		{
			this.domBtDir = this.criarBotao(
				'Próximo',
				this.btDirClasse,
				'seta-dir',
				this.domBtDirLocal
			);
		};
  }

  private criarBotao(texto, classe, tipo, local) {
    let bt  = document.createElement('button'),
    txt = document.createTextNode(texto);
    bt.className = classe;
    bt.setAttribute('data-tipo', tipo);
    bt.appendChild(txt);
    bt.addEventListener('click', (evt) => { this.acaoMover(evt) });
    local.appendChild(bt);
    return bt;
  }

  //Define valor das propriedades
	definir(prop, val)
	{
		this[prop] = val;
		return this;
	}

	/**
	* Recebe um seletor do elemento no DOM
	* com os elementos
	* ----
	* @Retorno: Objeto - o Próprio
	**/
	defElementos( seletor )
	{
		this.domElementos = Seletor.lista( seletor );
		return this;
	}

  /**
  * Recebe um seletor do elemento no DOM do
  * grupo que contem os elementos
  * ----
  * @Retorno: Objeto - o Próprio
  **/
  defGrupo( seletor )
  {
    this.domGrupo = <HTMLElement>Seletor.item( seletor );
    return this;
  }

  /**
	* Recebe um seletor do elemento no DOM de janela
	* ----
	* @Retorno: Objeto - o Próprio
	**/
	defJanela( seletor )
	{
		this.domJanela = <HTMLElement>Seletor.item( seletor );
		return this;
	}

  //Define valor das propriedades
  elemento(domElem, val)
  {
    domElem = Seletor.item( val );
    return this;
  }

  //Associa os eventos e cria elementos
  instalar() {
    if( this.situacaoInstalado ) return;

    // Calculando espaços
		this.ajustar();

		//Criando setas
		this.criarSetas();

		//Criando botões de grupo
		this.criarBtsGrupo();

    // Solicitando aviso quando a janela for redimensionada
		window.addEventListener('resize', () => this.ajustar());

		this.situacaoInstalado = true;
  }

  /**
	* Realiza a animaçaõ de movimento
	* ----
	* @Entrada: rolagem - posição para onde de vir
	* @Retorno: O próprio objeto
	**/
	mover(rolagem)
	{
		//Animando
		this.Animar = new Animar(
			this.domJanela,
			{'scrollLeft': rolagem},
			this.velocidade
		);

		return this;
	}

  /**
	* Rola o carrosel para direita
	* ----
	* @Entrada: distancia - INT - Tamanho do pulo
	* @Retorno: O próprio objeto
	**/
	moverParaDireita( distancia? )
	{
		let pulo = distancia ? distancia : this.pulo
		let rolagem = this.domJanela.scrollLeft + pulo;

		this.analisarBotoesSetas( rolagem );

		this.mover( rolagem );

		return this;
	}

	/**
	* Rola o carrosel para esquerda
	* ----
	* @Entrada: distancia - INT - Tamanho do pulo
	* @Retorno: O próprio objeto
	**/
	moverParaEsquerda(distancia?)
	{
		let pulo = distancia ? distancia : this.pulo
		let rolagem = this.domJanela.scrollLeft - pulo;

		this.analisarBotoesSetas( rolagem );

		//Animando
		this.mover( rolagem );

		return this;
	}

  /**
	* Move elemento para o centro
	* ----
	* @Retorno: Objeto - o próprio
	**/
	moverParaCentro()
	{
		let indice = this.elementoIndice;
		let elem = this.domElementos[ indice ];

		let elementoMeio = Math.round(this.domElementos.length - (this.domElementos.length / 2) -0.1);

		elem = this.domElementos[elementoMeio];
		this.elementoIndice = elementoMeio;

		this.identificaCentro(elem);

		let distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

		this.mover( distancia );
		this.avise("carrossel-instalado");
	}

  /**
  * Move para o próximo elemento
  * ----
  * @Retorno: Objeto - o próprio
  **/
  moverParaProximo()
  {
    this.elementoIndice++;
    var indice = this.elementoIndice,
    elem = this.domElementos[ indice ];


    if( !elem ) {
      elem = this.domElementos[0];
      this.elementoIndice = 0;
    }

    this.identificaCentro(elem);

    var distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

    this.mover( distancia );
    this.avise( 'movido-proximo', elem );
  }

  /**
  * Move para o elemento anterior
  * ----
  * @Retorno: Objeto - o próprio
  **/

  moverParaAnterior()
  {
    this.elementoIndice--;
    var indice = this.elementoIndice,
    elem = this.domElementos[ indice ];


    var ultimoElemento = this.domElementos.length - 1;

    console.log(elem);

    if( !elem ) {
      elem = this.domElementos[ultimoElemento];
      this.elementoIndice = ultimoElemento;
    }

    this.identificaCentro(elem);

    var distancia = elem.offsetLeft + elem.offsetWidth / 2 - this.espaco / 2;

    this.mover( distancia );
    this.avise( 'movido-anterior', elem );
  }

  /**
  *Adiciona classe ao elemento que está no meio
  **/

  identificaCentro(elem)
  {
    for (var i = 0; i < this.domElementos.length; i++) {
      this.domElementos[i].classList.remove("centro")
    }
    elem.classList.add("centro")
  }

  /**
  * Retorna os elementos selecionados
  * ----
  * @Retorno: ARRAY - lista de elementos
  **/
  obterElementos()
  {
    return this.domElementos;
  }

  /**
  * Ativa ou Desativa a rolagem automática
  * ----
  * @Entrada: INT - Tempo e milisegundos
  * @Efeito: Desliza par ao próximo elemento
  * @Retorno: Objeto - O próprio
  **/
  rolagemAutomatica( tempo, pausa:boolean )
  {
    this._relogio_intervalo = parseInt( tempo );
    this._relogio_pausa = !!pausa;

    if( !tempo && this._relogio_ref )
    {
      window.clearInterval( this._relogio_ref );
      return this;
    }

    // Configurando eventos para caso de pausas
    if( this._relogio_pausa )
    {
      this.domGrupo.addEventListener( 'mouseover', () => { this._relogio_pausado = true; } );
      this.domGrupo.addEventListener( 'mouseout', () => { this._relogio_pausado = false; } );
    }

    this._relogio_ref = window.setInterval( () =>
    {
      if( !this._relogio_pausado ) this.moverParaProximo();
    }, tempo );

    return this;
  }

  /**
  * Ação ao clicar em botões de seta e/ou grupo
  * ----
  * @Entrada: evt - Evento
  **/
  private acaoMover(evt)
  {
    var rolagem,
        elem = evt.target,
        tipo = elem.getAttribute('data-tipo');

    //Pegando posição atual
    rolagem = this.domJanela.scrollLeft;

    //Vendo qual botão foi acionado
    if(tipo == 'grupo')
    {
      rolagem = (elem.getAttribute('data-posicao') - 1) * this.grupoPulo;

      // Removendo classe de elementos
      this.domBtsGrupo.forEach((e) => {
        e.classList.remove('ativo');
      });

      // Adicionando no atual
      elem.classList.add('ativo');

      //Cancelando ação padrão do link
      evt.preventDefault();

      this.analisarBotoesSetas( rolagem );
      this.mover( rolagem );
    }
    else if(tipo == 'seta-esq')
    {
      evt.preventDefault();
      return this.moverParaEsquerda();
    }
    else if(tipo == 'seta-dir')
    {
      evt.preventDefault();
      return this.moverParaDireita();
    };
  }

}
