var Ciebit;
(function (Ciebit) {
    var Js;
    (function (Js) {
        var Sanfona = (function () {
            function Sanfona(area, botao) {
                this.area = area;
                this.botao = botao;
                this.classeAberta = 'cb-sanfona-aberta';
                this.classeFechada = 'cb-sanfona-fechada';
                this.classeBotaoAbrir = 'cb-sanfona-botao-abrir';
                this.classeBotaoFechar = 'cb-sanfona-botao-fechar';
                this.id = 'cb-sanfona-' + Sanfona.contador++;
                this.armarGatilho();
            }
            Sanfona.prototype.abrir = function () {
                this.area.classList.remove(this.classeFechada);
                this.area.classList.add(this.classeAberta);
                if (this.botao) {
                    this.botao.classList.remove(this.classeBotaoAbrir);
                    this.botao.classList.add(this.classeBotaoFechar);
                }
                return this;
            };
            Sanfona.prototype.armarGatilho = function () {
                var _this = this;
                this.area.classList.add(this.classeAberta);
                if (!this.botao) {
                    return this;
                }
                this.botao.classList.add(this.classeBotaoFechar);
                this.botao.addEventListener('click', function () { return _this.alternar(); });
                return this;
            };
            Sanfona.prototype.alternar = function () {
                if (this.area.classList.contains(this.classeAberta)) {
                    this.fechar();
                }
                else {
                    this.abrir();
                }
                return this;
            };
            Sanfona.prototype.fechar = function () {
                this.area.classList.remove(this.classeAberta);
                this.area.classList.add(this.classeFechada);
                if (this.botao) {
                    this.botao.classList.remove(this.classeBotaoFechar);
                    this.botao.classList.add(this.classeBotaoAbrir);
                }
                return this;
            };
            return Sanfona;
        }());
        Sanfona.contador = 0;
        Js.Sanfona = Sanfona;
    })(Js = Ciebit.Js || (Ciebit.Js = {}));
})(Ciebit || (Ciebit = {}));
