/*
==================================
CIEBIT
www.ciebit.com.br
suporte@ciebit.com.br
==================================
Objeto Galeria
----------------------------------

Dependências:
- cb.s.addEventListener
- cb.f.hermes
- cb.f.classes
- cb.f.seletor
- cb.o.visualizador
- cb.o.ajax
*/


// Galeria
class Galeria {
	private static contador:number;
	private objId:string;

	private api:string;
	private domArea:HTMLElement;
	private domItens:Array<HTMLLinkElement>;
	private domAnterior:HTMLLinkElement;
	private domSelecionada:HTMLLinkElement;
	private domProxima:HTMLLinkElement;
	private elemClasse:string;
	private instalado:boolean;

	// Objetos
	private Api:Api;
	private Hermes:Ciebit.Hermes;
	private Visualizador:Visualizador;

	/*
	* Configura o objeto
	*/
	public construct()
	{
		this.objId = 'cb-galeria-'+ Galeria.contador++;

		this.elemClasse = 'cb-gal_item';
		this.instalado = false;

		this.Hermes = new Ciebit.Hermes;
		this.Visualizador = new Visualizador;
	}

	/*
	* Atrela funções a eventos
	*/
	public aviseMe( evento:string, funcao:Function ):this
	{
		this.Hermes.aviseMe( evento, funcao );
		return this;
	}

	/*
	* Método:  Define a classe de cada item da galeria
	* ----
	* @recebe:  String com o nome da classe
	* @retorna: O própiro objeto
	* @efeito:  Atrela propriedades
	*/
	public set classe( classe:string )
	{
		this.elemClasse = classe;
	}

	/*
	* Define o caminho da api
	*/
	public defApi( url:string ):this
	{
		this.api = url;
		return this;
	}

	/*
	* Verifica eventos ocorrido na área da galeria
	*/
	private evtClique( evt:Event ):void
	{
		var sit = false, ref = <any> evt.target;

		// Buscando referencia
		do{
			// Se igual a área encerrar
			if( ref == this.domArea ) return;

			// Se houver a classe o objeto foi encontrado
			if( ref.classList.contains( this.elemClasse ) )
			{
				sit = true;

				//Cancela direcionamento padrão
				evt.preventDefault();
				break;
			}
		}
		while( ref = <any> ref.parentNode );

		// Se não houver encontrado encerra
		if( !sit ) return;

		// Se não for um link encerrar
		if( !(ref instanceof HTMLLinkElement) ) return;

		// Pegando posição do item
		var pos = this.domItens.indexOf( ref ) +1;

		//Selecionando elementos
		this.selItem( pos );

		// Tranferindo par ao visualizador
		this.transferirParaViz();
	}

	/*
	* Função:  Vincular evento aos Elementos Dom
	* ----
	* @recebe:  domArea - STRING/ELEMENTO refente a área da galeria
	* @retorna: Nada
	* @efeito:  Atrela eventos
	*/
	public instalar( domArea:HTMLElement ):this
	{
		//Encerrar se já houver sido instalado
		if( this.instalado ) return;

		//Definindo área
		this.domArea = domArea;

		//Evento ao clicar
		this.domArea.addEventListener( 'click', ( e )=>{ this.evtClique( e ); } );

		this.selecionarFilhos();

		// Adicionando evento dos botões avancar e retorceder
		this.Visualizador.aviseMe(
			'anterior', (e) => {
				e.preventDefault();
				this.selProxima();
				this.transferirParaViz();
			}
		).aviseMe(
			'proximo', (e) => {
				e.preventDefault();
				this.selAnterior();
				this.transferirParaViz();
			}
		).instalar();

		//Informando instalação
		this.instalado = true;
		this.Hermes.avise( 'instalado' );

		return this;
	}

	/*
	 * Busca no servidor dados da imagem solicitada
	*/
	public obterDados( id:number )
	{
		let PedDados = new Ciebit.Api.Pedido( 'obter-dados' );
		PedDados.paramAdi( 'id', id );

		this.Api = new Api( this.api, [ PedDados ] );
		this.Api.enviar( ( dados ) => this.receberDados( dados ) );

		//Informando a solicitação dos dados
		this.Hermes.avise( 'dados-solicitados' );
	};

	/*
	 * Trato o valor recebido
	*/
	private receberDados( Msg )
	{
		// Verificando se houve falhas
		if( !Msg.situacao() )
		{
			this.Hermes.avise( 'obter-dados-erro' );
			window.open( this.domSelecionada.href, this.domSelecionada.target );
			return;
		}

		// Passando informações para o visualizador
		this.Visualizador.titulo = Msg.dados.titulo;
		this.Visualizador.descricao = Msg.dados.legenda;
		this.Visualizador.defImagem(
			Msg.dados.url,
			Msg.dados.largura,
			Msg.dados.altura,
			Msg.dados.descricao
		).
		abrir();

		// Informando recebimento dos dados
		this.Hermes.avise( 'dados-recebidos', Msg.dados );
	}

	/*
	* Seleciona o elemento anterior ao atual
	*/
	public selAnterior():boolean
	{
		var pos = this.domItens.indexOf( this.domAnterior ) +1;
		return this.selItem( pos );
	};

	/*
	* Seleciona elementos com a classe especificada
	*/
	public selecionarFilhos():this
	{
		let filhos = <NodeListOf<HTMLLinkElement>> this.domArea.querySelectorAll( '.'+ this.classe );

		// Se não houver filhos encerrar
		if( !filhos ) return;

		// Contando o total de elementos
		let tot = filhos.length;

		// Verificando tipo de elementos
		this.domItens = [];
		for(var i = 0; i < tot; i++) this.domItens.push( filhos[i] );

		return this;
	}

	/*
	* Marca um elemento da galeria como o atual
	*/
	public selItem( posicao ):boolean
	{
		//Encerrar se não houver posição informada
		if( !this.domItens[posicao -1] ) return false;

		this.domSelecionada = undefined;
		this.domAnterior    = undefined;
		this.domProxima     = undefined;

		//Selecionado
		this.domSelecionada = this.domItens[ posicao -1 ];

		//Anterior
		if(this.domItens[posicao -2])
			this.domAnterior = this.domItens[posicao -2];

		//Próximo
		if(this.domItens[posicao])
			this.domProxima = this.domItens[posicao];

		return true;
	};

	/*
	* Seleciona o próximo elemento da galeria
	*/
	public selProxima()
	{
	var pos = this.domItens.indexOf( this.domProxima ) +1;
	return this.selItem( pos );
	};

	/*
	* Passa informações para o objeto Visualizador
	*/
	public transferirParaViz():boolean
	{
		// Verificando tipo de ação
		if( this.domSelecionada instanceof HTMLImageElement )
		{
			this.Visualizador.
			defImagem(
				this.domSelecionada.src,
				this.domSelecionada.width,
				this.domSelecionada.height,
				this.domSelecionada.alt
			).
			abrir();
		}
		else if( this.domSelecionada instanceof HTMLLinkElement )
		{
			// Verificando se há dados a serem coletados
			let id = this.domSelecionada.getAttribute('data-id');

			if( id ){
				this.obterDados( parseInt( id ) );
				return true;
			}

			// Exibindo imagem no href
			let alt = this.domSelecionada.querySelector('img').alt;

			this.Visualizador.
			defImagem(
				this.domSelecionada.href,
				undefined,
				undefined,
				alt
			).
			abrir();
		}
		else return false;

		// Alternando situação dos botões
		this.Visualizador.btProximoAtivo( !this.domProxima );
		this.Visualizador.btAnteriorAtivo( !this.domAnterior );

		return true;
	};
}
