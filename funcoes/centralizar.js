/*
==================================
			CIEBIT
==================================
Criada: 12/04/2014
Altera: 12/04/2014
----------------------------------
*/

/* ------------
	Centralizador
*/
//Posiciona elementos com base em outro elemento
cb.f.centralizar = function(elem, base, horz, vert)
{
	var obj  = {'x':0,'y':0}, //Coordenadas Objeto
		tela = {'x':0,'y':0}, //Coordenadas �rea
		calc = {'x':0,'y':0}, //Coordenadas Calculo
		larg = false, //Se posiciona horizonal
		altu = false, //Se posiciona vertical
		elemDisplay = elem.style.display,
		baseDisplay = base.style.display;

	//Exibindo os objetos caso estejam ocultos
	elem.style.display =
	base.style.display = 'block';

	//Se posciona horizontal
	if(typeof(horz) == 'undefined' || horz)
	{
		elem.style.left  = 
		elem.style.right = '';
		larg = true;
	}
	//Se posciona vertical
	if(typeof(vert) == 'undefined' || vert)
	{
		elem.style.top    = 
		elem.style.bottom = '';
		altu = true;
	}

	//Posicionamento horizontal
	if(larg)
	{
		obj.x  = parseInt(elem.clientWidth);
		obj.x  = !obj.x ? 0 : obj.x;
		
		tela.x = parseInt(base.clientWidth);
		tela.x = !tela.x ? 0 : tela.x;
		
		calc.x = (tela.x - obj.x) / 2;

		elem.style.left  = calc.x +'px';
		elem.style.right = calc.x +'px';
	}


	//Posicionamento vertical
	if(altu)
	{
		obj.y  = parseInt(elem.clientHeight);
		obj.y  = !obj.y ? 0 : obj.y;

		tela.y = base.clientHeight;
		tela.y = !tela.y ? 0 : tela.y;

		calc.y = (tela.y - obj.y) / 2;

		elem.style.top    = calc.y +'px';
		elem.style.bottom = calc.y +'px';
	}

	//Retornando ao display padr�o
	elem.style.display = elemDisplay;
	base.style.display = baseDisplay;

	return this;
}