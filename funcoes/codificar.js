/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

-
Recebe uma string e converte em um número
da base 32
--- */

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);

/*
* Converte uma string em um código
* ===
* @Recebe:
* -- STRING - Texto a ser convertido
* @Retorna:
* -- STRING - Código referente a string
*/
cb.f.codificar = function(texto)
{
	if(
		!texto ||
		!(texto.length)
	) {
		return '';
	}

	var i, codigo = '', tot = texto.length;

	for (i = 0; i < tot; i++) {
		var tmp = texto.charCodeAt(i);
		codigo += tmp.toString(32);
	}

	return codigo;
}
