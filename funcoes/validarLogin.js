/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

- 
Valida um texto que possua apenas 
letras, números, hífens e underlines
--- */
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);

cb.f.validarLogin = function(login)
{
	var reg = /[^0-9a-z_-]+/,
		txt = new String(login);
	return !reg.test(txt);
};