/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

- 
Valida um texto que possua apenas 
letras, números e hífens
--- */
var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);

cb.f.validarUri = function(uri)
{
	var reg = /[^0-9a-z-]+/,
		txt = new String(uri);
	return !reg.test(txt);
};