/*
==================================
			CIEBIT
==================================
Criada: 12/04/2014
Altera: 12/04/2014
----------------------------------
*/

cb.f.Animar = function(obj, param, velocidade)
{
	var prefixo = true;
	var val = {};
	var exp = /(\-?\d+)([\%\w\W]+)/;
	var quaSeg = 30; //Quadros por 

	//Capturando os valores atuais dos parametros informados
	for(var temp in param)
	{
		var expTemp = [];

		if(temp == 'left'){
			expTemp[1] = obj.offsetLeft;
			expTemp[2] = 'px';
		}
		else if(temp == 'scrollTop'){
			prefixo = false;
			expTemp[1] = obj.scrollTop;
			expTemp[2] = '';
		}
		else if(temp == 'scrollLeft'){
			prefixo = false;
			expTemp[1] = obj.scrollLeft;
			expTemp[2] = '';
		}
		else
			expTemp = exp.exec(obj.style[temp]);

		val[temp] = {};
		val[temp].prefixo     = prefixo;
		val[temp].propriedade = temp;
		val[temp].inicio      = parseInt(expTemp[1]);
		val[temp].decorrido   = val[temp].inicio;
		val[temp].fim         = param[temp];
		val[temp].unidade     = expTemp[2];
		val[temp].distancia   = val[temp].fim > val[temp].inicio ?
								val[temp].fim - val[temp].inicio :
								val[temp].inicio - val[temp].fim;
		val[temp].pulo        = val[temp].distancia / (velocidade / 20);

		Iniciar(obj, val[temp]);
	}

	//Fun�o de Start
	function Iniciar(obj, param)
	{
		var uma = false;
		//Executando anima��o
		var intervalo = window.setInterval(
			function()
			{
				if(
					(param.inicio < param.fim &&
					param.decorrido >= param.fim) ||
					(param.inicio >= param.fim &&
					param.decorrido <= param.fim)
				){
					window.clearInterval(intervalo);
					return;
				}

				if(param.fim > param.inicio)
				{
					param.decorrido = param.decorrido + param.pulo
					if(param.decorrido > param.fim) param.decorrido = param.fim;
				}
				else
				{
					param.decorrido = param.decorrido - param.pulo;
					if(param.decorrido < param.fim) param.decorrido = param.fim;
				}

				var calc = param.decorrido + param.unidade;

				if(param.prefixo){
					obj.style[param.propriedade] = calc;
				} else {
					obj[param.propriedade] = calc;
				}
			}, 
			20
		);
	}
}