/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

- 
Valida um e-mail
--- */

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);

cb.f.validarEmail = function(email)
{
	var reg = /^[\d\w\-\_]+[\d\w_\-\.]*[\d\w\-\_]+@[\d\w]+[\d\w\-_\.]*[\d\w]+\.[\d\w]+[\d\w\.\-_]*$/,
		txt = new String(email);
	return reg.test(txt);
};