/*
==================================
			CIEBIT
==================================
Criada: 22/04/2015
Altera: 22/04/2015
----------------------------------
*/

/* ----------------------------------------
 * Mescla Objetos
*/

cb.f.mesclar = function()
{
	var obj = {}, tot = arguments.length;

	// Percorrendo todos os argumentos
	for (var i = 0; i <= tot; i++) 
	{
		var objTmp = arguments[i];

		// Se n�o for objeto pular
		if(!(objTmp instanceof Object)) continue;

		// Percorrendo propriedades e m�todos
		for(var x in objTmp) 
		{
			// Se n�o houver pular
			if(!objTmp.hasOwnProperty(x)) continue;
			// Se j� houver pular
			if(obj.hasOwnProperty(x)) continue;

			obj[x] = objTmp[x];
		}
	};

	return obj;
}