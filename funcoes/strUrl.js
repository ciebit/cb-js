/*
==================================
			CIEBIT
==================================
Criada: 14/06/2014
Altera: 14/06/2014
----------------------------------
*/
cb.f.strUrl = function(texto)
{
	var txt = String(texto),
		reg,
		esp = [
			{'val':'a', 'crt': '[áàâäã]'},
			{'val':'e', 'crt': '[éèêë]' },
			{'val':'i', 'crt': '[íìîï]' },
			{'val':'o', 'crt': '[óòôöõ]'},
			{'val':'u', 'crt': '[úùôöõ]'},
			{'val':'c', 'crt': 'ç'},
			{'val':'-', 'crt': '\-{2,}'},
			{'val':'',  'crt': '[^0-9a-z\-]'},
		];

	txt = txt.toLowerCase();    //minúsculas
	txt = txt.trim();           //removendo espaços iniciais e finais
	txt = txt.replace(/\s/g, '-');//substituindo espaços entre palavras

	//Substituindo caracteres especiais
	for(var i=0, tot = esp.length; i < tot; i++)
	{
		reg = new RegExp(esp[i].crt, 'g');
		txt = txt.replace(reg, esp[i].val);
		reg = null;
	}

	return txt;
}