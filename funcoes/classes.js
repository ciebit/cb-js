/*
==================================
			CIEBIT
==================================
Referência: http://www.openjs.com/scripts/dom/class_manipulation.php
*/

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);
/*
 * Função: Adiciona uma classe ao elemento
 * Recebe:
 	elem	- Objeto DOM - Destino da classe
 	classe  - STRING     - Nome da clase
 * Retorna: Nada
 * Efeito: Altera o elemento no DOM
*/
cb.f.classeAdi = function(elem, classe)
{
	if(!cb.f.ClasseTem(elem, classe)) elem.className += ' '+ classe;
}

/*
 * Função: Adiciona classe se não houver e remove se houver
 * Recebe:
 	elem	- Objeto DOM - Elemento a ser verificado
 	classe  - STRING     - Nome da clase
 * Retorna: Nada
 * Efeito: Altera DOM
*/
cb.f.classeAlt = function(elem, classe)
{
	//Remover se houver
	if(cb.f.ClasseTem(elem, classe))
	{
		cb.f.ClasseExc(elem, classe);
		return;
	}

	//Se continuar adicionar
	cb.f.ClasseAdi(elem, classe);
};

/*
 * Função: Exclui classes de um elemento
 * Recebe:
 	elem	- Objeto DOM - Elemento alvo
 	classe  - STRING     - Nome da clase
 * Retorna: True se tiver removido ou se não houver
 * Efeito: Altera o elemento no DOM
*/
cb.f.classeExc = function(elem, classe)
{
	if(!cb.f.ClasseTem(elem, classe)) return true;

	var reg = new RegExp('(\\s|^)'+ classe +'(\\s|$)');
	elem.className = (elem.className.replace(reg, ' '));
	return true;
};

/*
 * Função: Recebe duas classes, remove a primeira e adiciona a segunda
 * Recebe:
 	elem	- Objeto DOM - Elemento alvo
 	classe1 - STRING     - Nome da classe 1
 	classe2 - STRING     - Nome da classe 2
 * Retorna: Um string com as classes do elemento
 * Efeito: Altera o elemento no DOM
*/
cb.f.classeExcAdi = function(elem, classe1, classe2)
{
	cb.f.classeExc(elem, classe1);
	cb.f.classeAdi(elem, classe2);
	return elem.className;
};

/*
 * Função: Recebe duas classes e substitue uma por outra
 * Recebe:
 	elem	- Objeto DOM - Elemento alvo
 	classe1 - STRING     - Nome da classe 1
 	classe2 - STRING     - Nome da classe 2
 * Retorna: A classe que ficou no elemento
 * Efeito: Altera o elemento no DOM
*/
cb.f.classeSub = function(elem, classe1, classe2)
{
	if(cb.f.classeTem(elem, classe1))
	{
		cb.f.classeExc(elem, classe1);
		cb.f.classeAdi(elem, classe2);
		return classe2;
	}
	if(cb.f.classeTem(elem, classe2))
	{
		cb.f.classeExc(elem, classe2);
		cb.f.classeAdi(elem, classe1);
		return classe1;
	}
	return false;
};

/*
 * Função: Verifica se uma classe existe no objeto
 * Recebe:
 	elem	- Objeto DOM - Elemento a ser verificado
 	classe  - STRING     - Nome da clase
 * Retorna: Booleano - True se houver
 * Efeito: Nada
*/
cb.f.classeTem = function(elem, classe)
{
	//Encerrar se não for um objeto
	if(typeof(elem) != 'object') return;
	var reg = new RegExp('(\\s|^)'+ classe +'(\\s|$)');
	return reg.test(elem.className);
};

/*
	Suporte
*/
cb.f.ClasseAdi = cb.f.classeAdi;
cb.f.ClasseAlt = cb.f.classeAlt;
cb.f.ClasseExc = cb.f.classeExc;
cb.f.ClasseTem = cb.f.classeTem;