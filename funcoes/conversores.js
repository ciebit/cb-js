/*
==================================
			CIEBIT
==================================
Criada: 14/06/2014
Altera: 14/06/2014
----------------------------------
*/

/*
	Converte um objeto em query string
*/
cb.f.ParaQueryString = function(dados)
{
	var ret = '';

	//Encerrar se n? for objeto
	if(typeof dados !== 'object') return false;

	//Percorrendo dados
	for(i in dados){
		ret += '&';
		ret += i +'='+ dados[i];
	}

	return ret.substr(1);
}