/*
==================================
------------- CIEBIT -------------
==================================
*/
cb.f.alinhar = function(elem, base, horz, vert)
{
  if( !elem || !base ) return;

	// Obtendo distancias dos objetos
	var obj = elem.getBoundingClientRect(),
		ref = base.getBoundingClientRect();

	// Verificando se existe alinhamento horizontal
	switch(horz)
	{
		case 'esquerda':
			var calc = elem.offsetLeft;
			calc += (ref.left - obj.left);
			elem.style.left = calc +'px';
			break;

		case 'centro':
			cb.f.alinharCentroX( elem, base );
			break;
	}

	switch(vert)
	{
		case 'topo':
			cb.f.alinharTopo( elem, base );
			break;

		case 'centro':
			cb.f.alinharCentroY( elem, base );
			break;

		case 'base':
			cb.f.alinharBase( elem, base );
			break;
	}
};

/**
* Alinha as bases de elementos
* ----
* @Entrada:
* - elem - Objeto a ser posicionado
* - base - Objeto referencia
* - respeitar - Booleano - true indica que não podem ficar posicionados sobre
* - margem - Int - número em pixels para indicar distancia após posicionamento
* @Efeito: Adiciona estilos inline
* @Retorno: Undefined
**/
cb.f.alinharBase = function( elem, base, respeitar, margem )
{
	// Obtendo distancias dos objetos
	var elemMedidas = elem.getBoundingClientRect(),
		baseMedidas = base.getBoundingClientRect(),
		calc = baseMedidas.bottom - elemMedidas.height;

	if( respeitar ) calc = calc - baseMedidas.height;
	if( margem ) calc = calc - margem;

	elem.style.top = calc +'px';
}

cb.f.alinharCorrecaoPelaPagina = function( elem )
{
	cb.f.alinharCorrecaoPelaPaginaX( elem );
	cb.f.alinharCorrecaoPelaPaginaY( elem );
}

cb.f.alinharCorrecaoPelaPaginaX = function( elem )
{
	var elemEsq = elem.offsetLeft,
	elemLarg = elem.offsetWidth,
	pagLarg = window.innerWidth;

	// Posicionando no eixo Y
	if( elemEsq < 0 ) elem.style.left = '0px';

	else if( ( elemLarg + elemEsq ) > pagLarg )
	{
		var calc = pagLarg - elemLarg;

		if( !calc ) calc = 0;

		elem.style.left = calc +'px';
	}
}

cb.f.alinharCorrecaoPelaPaginaY = function( elem )
{
	var elemTop = elem.offsetTop,
	elemAlt = elem.offsetHeight,
	pagAlt = window.innerHeight;

	// Posicionando no eixo Y
	if( !elemTop ) elem.style.top = '0px';
	else if( ( elemAlt + elemTop ) > pagAlt )
	{
		var calc = pagAlt - elemAlt;

		if( !calc ) calc = 0;

		elem.style.top = calc +'px';
	}
}

/**
* Alinha um elemento com base em outro no eixo X
* ----
* @Entrada:
* - elem - Objeto a ser posicionado
* - base - Objeto referencia
* - contido - Booleano - true indica que não deve ser posicionado fora da página
* @Efeito: Adiciona estilos inline
* @Retorno: Undefined
**/
cb.f.alinharCentroX = function( elem, base, contido )
{
	var ideal, diferenca, baseMedidas, elemMedidas;

	// Zerando a esquerda
	elem.style.left = 0;

	// Pegando medidas
	baseMedidas = base.getBoundingClientRect();
	elemMedidas = elem.getBoundingClientRect();

	// Calculando posicao correta
	diferenca = (baseMedidas.width - elemMedidas.width) / 2;
	ideal = baseMedidas.left - elemMedidas.left + diferenca;

	// Posicionando
	elem.style.left = parseInt( ideal ) +'px';

	// Verifica se ficou fora da página
	if( contido ) cb.f.alinharCorrecaoPelaPaginaX( elem );
}

/**
* Alinha um elemento com base em outro no eixo Y
* ----
* @Entrada:
* - elem - Objeto a ser posicionado
* - base - Objeto referencia
* - contido - Booleano - true indica que não deve ser posicionado fora da página
* @Efeito: Adiciona estilos inline
* @Retorno: Undefined
**/
cb.f.alinharCentroY = function( elem, base, contido )
{
	var ideal, diferenca, baseMedidas, elemMedidas;

	// Zerando o topo
	elem.style.top = 0;

	// Pegando medidas
	baseMedidas = base.getBoundingClientRect();
	elemMedidas = elem.getBoundingClientRect();

	// Calculando posicao correta
	diferenca = (baseMedidas.height - elemMedidas.height) / 2;
	ideal = baseMedidas.top - elemMedidas.top + diferenca;

	// Posicionando
	elem.style.top = parseInt( ideal ) +'px';

	// Verifica se ficou fora da página
	if( contido ) cb.f.alinharCorrecaoPelaPaginaY( elem );
}

/**
* Alinha elemento com base em outro pelo eixo Y no topo
* ----
* @Entrada:
* - elem - Objeto a ser posicionado
* - base - Objeto referencia
* - respeitar - TRUE - caso o objeto a ser posicionado nao deva ficar sobre o objeto de referencia
* - margem - distancia em pixels do objeto base
* @Efeito: Adiciona estilos ao alemento
* @Retorno: undefined
**/
cb.f.alinharTopo = function( elem, base, respeitar, margem )
{
	var calc, baseMedidas, elemMedidas;

	// Pegando medidas
	baseMedidas = base.getBoundingClientRect();
	elemMedidas = elem.getBoundingClientRect();

	// Calculando
	calc = elem.offsetTop;
	calc += ( baseMedidas.top - elemMedidas.top );

	// Incrementos
	if( respeitar ) calc += baseMedidas.height;
	if( margem ) calc += margem;

	// Aplicando
	elem.style.top = calc +'px';
}
