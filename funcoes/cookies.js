/*
==================================
			CIEBIT
==================================
*/

/*
 * Função: Adiciona um Cookie
 * Recebe:
 	nome  - STRING   - Nome do Cookie
 	valor - STRING   - Valor do cookie
 	data  - OBJ Data - Data de expiração
 * Retorna: Nada
 * Efeito: Criar um cookie
*/
cb.f.CookieSalvar = function(nome, valor, objData)
{
	document.cookie = nome +'='+ valor;
}

/*
 * Função: Exclui um cookie
 * Recebe:
 	nome	- STRING - Nome do Cookie
 * Retorna: Nada
 * Efeito: Remove um cookie
*/
cb.f.CookieExcluir = function(nome)
{
	var objData = new Date;
	document.cookie = nome +'=""; expires='+ objData.toUTCString();
};

/*
 * Função: Verifica se um cookie existe no objeto
 * Recebe:
 	nome	- STRING - Nome do Cookie
 * Retorna: Booleano - True se houver
 * Efeito: Nada
*/
cb.f.CookieTestar = function(nome)
{
	var reg = new RegExp('^[\\s;]*'+ nome +'\=');
	return reg.test(document.cookie);
};

/*
 * Função: Retorna o valor do cookie
 * Recebe:
 	Nome - STRING - Nome do Cookie
 * Retorna: Valor do Cookie
 * Efeito: Nada
*/
cb.f.CookieObter = function(nome)
{
	var reg = new RegExp('(^|[\\s;])'+ nome +'\=([\\w\\d\\s\\-\\_]+)(;|$)');
	var ret = reg.exec(document.cookie);
	return Array.isArray(ret) ? ret[2] : false;
};