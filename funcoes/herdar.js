/* ---
Ciebit
www.ciebit.com.br
suporte@ciebit.com.br

-
Faz um objeto herdar de outro
Obtido em: http://blog.caelum.com.br/reaproveitando-codigo-com-javascript-heranca-e-prototipos/
--- */

var cb;
if(!cb)   cb   = new Object(null);
if(!cb.f) cb.f = new Object(null);

cb.f.herdar = function(mae, filha)
{
	// Faz uma cópia do prototipo da mãe
	var copiaDaMae = Object.create(mae.prototype);

	// herda mãe
	filha.prototype = copiaDaMae;

	//Ajusta construtor da filha
	filha.prototype.constructor = filha;
}
