/*
==================================
------------- CIEBIT -------------
==================================
Dependências
- s.Array.indexOf
*/

var cb;
if(!cb)   cb   = {};
if(!cb.f) cb.f = {};

/*
	Comunica eventos
*/
cb.f.hermes = (function()
{
	var self      = this;
	var contador  = 0;
	var refOculta = [];

	return {
		//Propriedades
		'solicitacoes': {}, //Lista de ações
		'referencias': {},

		/*
		 * Função: Atrela uma ação a uma função
		 * Recebe:
		 	_funcao	- FUNÇÃO - Função a ser executada quando ocorrer a ação
		 	_acao	- STRING - Indentifiação da ação
		 	_ref	- STRING - Referencia da função para exclusão posterior
		 	_unica	- BOOLEAN- Informa se a função deve ser chamada apenas uma vez
		 * Retorna:
		 	Nada
		 * Efeito:
		 	Nada
		*/
		'aviseMe': function(_funcao, _acao, _ref, _unica)
		{
			var x = 'func'+ contador;

			//Criando item caso não exista
			if(!this.solicitacoes[_acao])
				this.solicitacoes[_acao] = {};

			//Adicinando função
			this.solicitacoes[_acao][x] = _funcao;

			//Adicionando referencia caso haja
			if(_ref) this.referencias[_ref] = {'acao': _acao, 'chave': x};

			//Caso deva ser chamado uma única vez
			if(_unica) refOculta.push(x);

			contador++;
		},

		/*
		 * Função: Executar funções que tenham solicitado avisos
		 * Recebe:
		 	_acao	- STRING	- Indentificação da ação
		 * Retorna:
		 	Nada
		 * Efeito:
		 	Chama Funções
		*/
		'informe': function(_acao)
		{
			// Verifica se existe alguma função para o aviso
			if(!this.solicitacoes[_acao]) return;

			// Percorrendo todas as solicitações
			for(var i in this.solicitacoes[_acao])
			{
				// Chamando função
				var args = Array.prototype.slice.call(arguments, 1);
				this.solicitacoes[_acao][i].apply(this, args);

				// Verificando se só deseja uma chamada
				var x = refOculta.indexOf(i);
				if(x >= 0)
				{
					// Removendo função para evitar ser
					// chamada novamente
					delete this.solicitacoes[_acao][i];
					delete refOculta[x];
				}
			}
		},

		/*
		 * Função: Remove uma função da lista de informes
		 * Recebe:
		 	_ref	- STRING	- Referência
		 * Retorna:
		 	Nada
		 * Efeito:
		 	Remove um item da lista de funções
		*/
		'esquecaMe': function(_ref)
		{
			var acao, chave;

			if(!this.referencias[_ref]) return;

			acao  = this.referencias[_ref].acao;
			chave = this.referencias[_ref].chave;

			delete this.solicitacoes[acao][chave];
			delete this.referencias[_ref];
		},
	}
})();

cb.f.hermes.avise = cb.f.hermes.informe;

//Informando que a página foi carregada
window.onload = function(){
	cb.s.paginaCarregada = true;
	cb.f.hermes.informe('pagina-carregada');
};
