/*
==================================
			CIEBIT
==================================
Dependências
- Nenhuma
*/
var cb;
if(!cb) cb = {};
if(!cb.f) cb.f = {};


/*
 * Função: Substitue o texto de um elemento dom pelo definido
 * Recebe:
 	elem  - Objeto DOM - Elemento a ser inserido o texto
 	texto - STRING     - Texto a ser inserido
 * Retorna: Booelano TRUE em caso de sucesso
 * Efeito: Altera DOM
*/
cb.f.textoAdi = function(ele, texto) 
{
	// Se não for um elemento válido encerrar
	if(!(ele instanceof Node)) return false;

	// Adicionado texto
	ele.appendChild(document.createTextNode(texto));

	return true;
}

/*
 * Função: Remove o texto de um elemento dom
 * Recebe:
 	elem  - Objeto DOM - Elemento a ter o texto removido
 * Retorna: Booelano TRUE em caso de sucesso
 * Efeito: Altera DOM
*/
cb.f.textoExc = function(ele) 
{
	// Se não for um elemento válido encerrar
	if(!(ele instanceof Node)) return false;

	// Removendo todos os elementos
	var filho;
	while(filho = ele.lastChild) ele.removeChild(filho);

	return true;
}

/*
 * Função: Substitue o texto de um elemento dom pelo definido
 * Recebe:
 	elem  - Objeto DOM - Elemento a ser inserido o texto
 	texto - STRING     - Texto a ser inserido
 * Retorna: Booelano TRUE em caso de sucesso
 * Efeito: Altera DOM
*/
cb.f.textoSub = function(ele, texto) 
{
	// Se não for um elemento válido encerrar
	if(!(ele instanceof Node)) return false;

	// Removendo todos os elementos
	cb.f.textoExc(ele);

	// Adicionado texto
	cb.f.textoAdi(ele, texto);

	return true;
}