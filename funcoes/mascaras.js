/*
==================================
------------- CIEBIT -------------
==================================
Criada: 12/04/2014
Altera: 12/04/2014
----------------------------------
*/

/* ----------------------------------------
 * Máscaras
*/

cb.f.mascaras = {};

//Filtro de teclas especiais
cb.f.mascaras.filtrarTeclas = function(event)
{
	var _bts = [8,35,36,37,39,46,16,17];

	if(_bts.indexOf(event.which) >= 0) return false;

	return true;
}

//Máscara de CEP
cb.f.mascaras.cep = function(_val)
{
	var ret = '',
		dad = new String(_val),
		cep = dad.replace(/\D/g, ''),
		tot = cep.length;

	if(tot >= 6) ret = '-'+ cep.substr(5,3);
	if(tot >= 3) ret = '.'+ cep.substr(2,3) + ret;
	if(tot >= 1) ret = cep.substr(0,2) + ret;

	return ret;
}

//Máscara de CNPJ
cb.f.mascaras.cnpj = function(_val)
{
	var _ret  = '',
		_dad  = new String(_val),
		_cnpj = _dad.replace(/\D/g, ''),
		_tot  = _cnpj.length;

	if(_tot >= 13) _ret = '-'+ _cnpj.substr(12,2);
	if(_tot >= 9)  _ret = '/'+ _cnpj.substr(8,4) + _ret;
	if(_tot >= 6)  _ret = '.'+ _cnpj.substr(5,3) + _ret;
	if(_tot >= 3)  _ret = '.'+ _cnpj.substr(2,3) + _ret;
	if(_tot >= 1)  _ret = _cnpj.substr(0,2) + _ret;

	return _ret;
}

//Máscara de CPF
cb.f.mascaras.cpf = function(_val)
{
	var _ret = '',
		_dad = new String(_val),
		_cpf = _dad.replace(/\D/g, ''),
		_tot = _cpf.length;

	if(_tot >= 10) _ret = '-'+ _cpf.substr(9,2);
	if(_tot >= 7)  _ret = '.'+ _cpf.substr(6,3) + _ret;
	if(_tot >= 4)  _ret = '.'+ _cpf.substr(3,3) + _ret;
	if(_tot >= 1)  _ret = _cpf.substr(0,3) + _ret;

	return _ret;
}

//Mascara de Telefone
cb.f.mascaras.telefone = function(tel)
{
	if(!tel) return '';

	var _val = new String(tel);
	var _num = _val.replace(/\D/g, '');
	var _pad = 1; //Padrão
	var _ret = '';

	//Caso nulo
	if(_num.length == 0) return '';
	else if(_num.length >= 11) _pad = 2;

	switch(_pad)
	{
		case 1:
			_ret =  '('+ _num.substr(0,2) +
					') '+ _num.substr(2,4) +
					'-'+ _num.substr(6,4);
			break;
		case 2:
			_ret =  '('+ _num.substr(0,2) +
					') '+ _num.substr(2,5) +
					'-'+ _num.substr(7,4);
			break;
		default:
			_ret = _num;
	}

	return _ret;
}

// Máscara de data
cb.f.mascaras.data = function(data)
{
	var _val = new String(data),
		_num = _val.replace(/\D/g, ''),
		_ret;

	//Caso nulo
	if(_num.length == 0) return '';

	_ret = _num.substr(0,2) +
		   '/'+ _num.substr(2,2) +
		   '/'+ _num.substr(4,4);

	return _ret;
}

// Máscara de moeda
cb.f.mascaras.moeda = function( moeda, prefixo )
{
	if( prefixo === undefined ) prefixo = 'R$ ';

	// Convertendo valor, em caso de erro retornar 0
	var moeda = parseFloat( moeda );
	if( !moeda ) return prefixo +'0,00';

	// Separando centavos e real
	var numeros = moeda.toString().split('.'),
	real = numeros[0], centavos = numeros[1];

	// Verificando real
	var tmp = '';
	for( var i = -1, grupo = 1; i >= (real.length * -1); i--, grupo++  )
	{
		var num = real.substr( i, 1 );
		if( grupo > 3)
		{
			tmp = '.'+ tmp;
			grupo = 1;
		}
		tmp = num + tmp;
	}
	if( tmp ) real = tmp;

	// Verificando centavos
	if( !centavos ) centavos = '00'
	else if( centavos < 10) centavos = '0' + centavos;

	return prefixo + real +','+ centavos
}

// Instalador de Telefone
cb.f.mascaras.InstalarTelefone = function(campo)
{
	campo.addEventListener('keyup', function(evt)
	{
		var tot = this.value.length,
			pos = 0;
		if(!cb.f.mascaras.filtrarTeclas(evt)) return;
		if(this.selectionStart) pos = this.selectionStart;

		//Aplicando máscara
		this.value = cb.f.mascaras.telefone(this.value);

		//Calculando posição
		tot -= pos;
		pos = this.value.length - tot;

		//Alterando posição do cursor
		this.selectionStart =
		this.selectionEnd   = pos;
	});
}

// Instalador de máscaras automáticas
cb.f.mascaras.Instalar = function(campo, mascara)
{
	campo.addEventListener('keyup', function(evt)
	{
		var tot = this.value.length,
			pos = 0;
		if(!cb.f.mascaras.filtrarTeclas(evt)) return;
		if(this.selectionStart) pos = this.selectionStart;

		//Aplicando máscara
		switch(mascara)
		{
			case 'cep':
				this.value = cb.f.mascaras.cep(this.value);
				break;
			case 'cnpj':
				this.value = cb.f.mascaras.cnpj(this.value);
				break;
			case 'cpf':
				this.value = cb.f.mascaras.cpf(this.value);
				break;
			case 'telefone':
				this.value = cb.f.mascaras.telefone(this.value);
				break;
		}

		//Calculando posição
		tot -= pos;
		pos = this.value.length - tot;

		//Alterando posição do cursor
		this.selectionStart =
		this.selectionEnd   = pos;
	});
}
