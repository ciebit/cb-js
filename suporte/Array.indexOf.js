/*
==================================
			CIEBIT
==================================
Criada: 05/08/2014
Altera: 05/08/2014
----------------------------------
Função: 
	Criar o método indexOf caso o navegador não suporte
	
Dependências:
	- Nenhum
*/

/*
	Polyfill - Suporte Array.indexOf
	Obtido em: http://devdocs.io/javascript/global_objects/array/indexof
*/
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement , fromIndex) {
    var i,
        pivot = (fromIndex) ? fromIndex : 0,
        length;

    if (!this) {
      throw new TypeError();
    }

    length = this.length;

    if (length === 0 || pivot >= length) {
      return -1;
    }

    if (pivot < 0) {
      pivot = length - Math.abs(pivot);
    }

    for (i = pivot; i < length; i++) {
      if (this[i] === searchElement) {
        return i;
      }
    }
    return -1;
  };
}