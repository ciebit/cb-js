/*
==================================
			CIEBIT
==================================
Criada: 05/08/2014
Altera: 05/08/2014
----------------------------------
Função: 
	Criar um objeto FormData caso o navegador não suporte
	
Dependências:
	- conversores.js - (Função) ParaQueryString
*/

(function(){
	//Verifica já existe
	if(window.FormData) return;

	var dados = {};

	//Criando elementos
	FormData = function() 
	{
		this.form = undefined;
	}

	/*
	 * Função:  Armazena par nome/valor
	 * Recebe: 
	 	- nome  - STRING     - propriedade
	 	- valor - STRING/INT - valor do objeto
	 * Retorna: Nada
	 * Efeito:  Alimenta variavel interna
	*/
	FormData.prototype.append = function(nome, valor)
	{
		dados[nome] = valor;
	};

	/*
	 * Função:  Retorna String dos dados
	 * Recebe:  Nada
	 * Retorna: Query String dos dados armazenados
	 * Efeito:  Nada
	*/
	FormData.prototype.toString = function()
	{
		return cb.f.ParaQueryString(dados);
	};
})();

