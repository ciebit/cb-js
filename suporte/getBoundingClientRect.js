/*
==================================
			CIEBIT
==================================
Criada: 15/05/2015
Altera: 15/05/2015
----------------------------------
Dependências:
	- cbBase;
	- f.hermes;
*/

// Suporte a getBoundingClientRect
(function()
{
	if(Element.prototype.getBoundingClientRect)
	{
		var d, m, f;
		d = document.createElement('div');
		m = d.getBoundingClientRect();
		if(('width' in m) && ('height' in m)) return;
	};

	// Referência:
	// 	Livro:   JavaScript - O guia Definitivo
	// 	Autor:   David Flanagem
	//	Editora: bookman
	//  O código abaixo foi adaptado
	Element.prototype.getBoundingClientRect = function()
	{
		var x = 0, y = 0, l = 0, a = 0;

		// Somando possição até a raiz do documento
		for(var e = this; e != null; e = e.offsetParent)
		{
			x += e.offsetLeft;
			y += e.offsetTop;
		}

		// Subtraindo as barras de rolagem se houver
		for(var e = this.parentNode; e != null && e.nodeType == 1; e = e.parentNode)
		{
			x -= e.scrollLeft;
			y -= e.scrollTop;
		}

		// Pegando as larguras e altura
		l = this.offsetWidth;
		a = this.offsetHeight;

		return {'left': x, 'top': y, 'width': l, 'height': a};
	}
	Element.prototype.getBoundingClientRect.cb = 'sim';
})();
