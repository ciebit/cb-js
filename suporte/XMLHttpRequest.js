/*
==================================
			CIEBIT
==================================
Dependências:
	- nenhum
*/

/*
	Polyfill - Suporte a XMLHttpRequest
	Obtido em: http://compatibility.shwups-cms.ch/en/polyfills/?&id=58
	Modificado pela Ciebit
*/
(function() {
	if(!window.XMLHttpRequest)
	{
		var AXOs = ['MSXML2.XMLHTTP.6.0', 'MSXML3.XMLHTTP', 'Microsoft.XMLHTTP', 'MSXML2.XMLHTTP.3.0'];
		var correctAXO = null;
		
		XMLHttpRequest = function() {
			if (correctAXO === null) {
				var xhr;
				if (window.ActiveXObject) {
					for (var i = 0, c = AXOs.length; i < c; i++) {
						try {
							xhr = new window.ActiveXObject(AXOs[i]);
						} catch (e) { xhr = false; }
						if (xhr) {
							correctAXO = AXOs[i];
							return xhr;
						}
					}
				}
				correctAXO = false;
			}
			if (correctAXO === false) {
				throw new Error('XMLHttpRequest não é suportado pelo navegador');
			}
			return new window.ActiveXObject(correctAXO);
		};
	} else {
		if(!('addEventListener' in window.XMLHttpRequest)){
			window.XMLHttpRequest.prototype.addEventListener = function(e, f){ this['on'+e] = f; }
		}
	}
}());