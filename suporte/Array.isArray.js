/*
==================================
			CIEBIT
==================================
*/

/*
	Polyfill - Suporte Array.isArray
	Obtido em: http://devdocs.io/javascript/global_objects/array/isarray
*/
if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}