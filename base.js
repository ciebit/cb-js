/*
==================================
------------- CIEBIT -------------
==================================
*/

/*
	Objeto Geral da Ciebit
*/
var cb;
if(!cb)   cb   = {};
if(!cb.p) cb.p = {}; // parametros
if(!cb.o) cb.o = {}; // objetos
if(!cb.f) cb.f = {}; // funcoes
if(!cb.m) cb.m = {}; // modulos
if(!cb.s) cb.s = {}; // situações

(function( cb )
{
	var painel = document.documentElement.getAttribute('data-cb-caminho');
	var midias = document.documentElement.getAttribute('data-cb-midias');
	if( !painel ) painel = '/cb/';
	if( !midias ) midias = '/midias/';
	/*
	Parametros do sistema
	*/
	cb.p = {
		'painel': painel,
		'midias': midias,
	};

	/*
	Situações
	*/
	cb.s = {
		'paginaCarregada': false
	}
})( cb );
